<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Suscriptor;
use App\Mail\BoletinMail;
use App\Sermon;
// use Corcel\Model\Post;

class BoletinEnviarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'boletin:enviar
                            {--p|prueba : Envía un boletín de prueba al correo de admin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia masivamente email a suscriptores del boletín';

    // modelo Suscriptor
    private $suscriptores;

    /*
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Suscriptor $suscriptor)
    {
        parent::__construct();

        try {
            $this->suscriptores = $suscriptor->all();
        } catch (\Exception $ex) {
            $this->suscriptores = collect([]);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Sermon $sermon, Post $post)
    {
        $esPrueba = $this->option('prueba');

        $sermones = Sermon::with('lider', 'serie', 'medio')
            ->take(2)
            ->get();

        // Trae las dos entradas mas recientes del blog
        // $resultadosWP = Post::with('author', 'thumbnail')
        //     ->newest()
        //     ->type('post')
        //     ->published()
        //     ->take(2)->get();

        $resultadosWP = [];
        $entradas = [];

        foreach($resultadosWP as $res) {
            $entradas[$res->ID] = [
                'titulo' => $res->post_title,
                'autor' => $res->author->display_name,
                'fecha' => $res->post_date,
                'imagen' => $res->thumbnail->attachment->guid,
                'link' => $res->guid,
                'resumen' => $res->excerpt,
            ];
        }

        if ($esPrueba) {
            \Mail::to(EMAIL_ADMIN)->send(new BoletinMail($this->suscriptores[0], $sermones, $entradas));
        } else {
            foreach ($this->suscriptores as $suscriptor)  {
                \Mail::to($suscriptor->email)->send(new BoletinMail($suscriptor, $sermones, $entradas));
                sleep(5);
            }
        }

        $this->info('Se envió el lote de boletines exitosamente.');

    }
}
