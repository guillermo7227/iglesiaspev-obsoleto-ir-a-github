<?php

namespace App\Helpers;

class Utils
{

    /**
     * Convierte la URL del medio audio en un URL raw
     *
     */
    public static function getMedioRawURL(string $url)
    {
        if (strpos($url, 'drive.google')) {
            $id = substr($url, strpos($url, 'id=') + 3);
            return 'http://docs.google.com/uc?export=open&id=' . $id;
        }

        return $url;
    }

    /**
     * Devuelve la Id de un video dada una url
     *
     * @param  string   $url
     * @return string   Id de video
     */
    public static function getVideoIdFromUrl(string $url)
    {
           // "http://www.youtube.com/watch?v=k6mFF3VmVAs"
           // "https://youtu.be/nrYbeJLLmgY"
           if (strpos($url, 'youtu.be')) {
               $indiceComienzo = strpos($url, 'youtu.be');
               return substr($url, $indiceComienzo + 8 + 1);
           } else if (strpos($url, 'watch?v=')) {
               $indiceComienzo = strpos($url, 'watch?v=');
               return substr($url, $indiceComienzo + 8, 11);
           }

           throw new \InvalidArgumentException('La Url que pasó no está soportada.');
    }


    /**
     * Pone cero (0) al número de capítulo para el campo 'versiculos' de 'sermones'
     *
     * @param  string   $capituloYVersiculos
     * @return string   Capitulo con cero adelante. ej. 021
     */
    public static function padCapitulo(string $capituloYVersiculos)
    {
        $str = $capituloYVersiculos;

        $capitulo = substr($str, 0, strpos($str,':'));

        $padded = str_pad($capitulo, 3, "0", STR_PAD_LEFT);

        return $padded;
    }

    /**
     * Pone cero (0) al número de versículo para el campo 'versiculos' de 'sermones'
     *
     * @param  string   $capituloYVersiculos
     * @return string   Versículo con cero adelante. ej. 021
     */
    public static function padVersiculo(string $capituloYVersiculos)
    {
        $str = $capituloYVersiculos;

        if (strpos($str, '-')) {
            $longitudVersiculo = strpos($str, '-') - (strpos($str, ':') + 1);
            $versiculo = substr($str, strpos($str, ':') + 1, $longitudVersiculo);
        } else {
            $versiculo = substr($str, strpos($str, ':') + 1);
        }

        $padded = str_pad($versiculo, 3, "0", STR_PAD_LEFT);

        return $padded;
    }

    public static function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);

        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);

        parse_str(request()->getQueryString(), $query);

        unset($query[$pageName]);

        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

}
