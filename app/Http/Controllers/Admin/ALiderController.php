<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sermon;
use App\Serie;
use App\Medio;
use App\Lider;
use App\Http\Requests\GuardarLiderRequest;

class ALiderController extends Controller
{
    public function index()
    {

        return view('sitio.admin.lideres.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $slug)
    {
        $lider = Lider::where('slug', $slug)
                      ->firstOrFail();

        return view('sitio.admin.lideres.edit', compact('lider'));
    }

    public function create()
    {
        return view('sitio.admin.lideres.create');
    }

    public function store(GuardarLiderRequest $request)
    {
        $datos = $request->validated();

        $registro = Lider::create($datos);

        if ($request->hasFile('imagen')) {
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $path = $request->file('imagen')->storeAs(
                'lideres', $registro->id .'.'. $extension
            );

            $registro->imagen = 'archivos/' . $path;
        }

        $registro->save();

        \Alert::toast('El registro fue creado exitosamente.', 'success');

        return redirect()->route('sermones.lideres.show', $registro->slug);
    }

    public function update(GuardarLiderRequest $request, string $slug)
    {
        $datos = $request->validated();

        $registro = Lider::where('slug', $slug)->firstOrFail();

        $registro->fill($datos);

        if ($request->hasFile('imagen')) {
            $extension = $request->file('imagen')->getClientOriginalExtension();
            $path = $request->file('imagen')->storeAs(
                'lideres', $registro->id .'.'. $extension
            );

            $registro->imagen = 'archivos/' . $path;
        }

        $registro->save();

        \Alert::toast('El registro fue actualizado exitosamente.', 'success');

        return redirect()->route('sermones.lideres.show', $registro->slug);
    }


    public function apiDestroy(string $slug)
    {
        $registro = Lider::where('slug', $slug)->firstOrFail();

        $registro->delete();

        \Alert::toast('El registro fue eliminado exitosamente.', 'success');

        return response()->json([
            'exito' => 'Se eliminó el registro exitosamente.'
        ]);
    }


    public function apiTodos(Request $request)
    {
        $registros = null;

        // toma el valor del argumento 'sort', orden
        $reqsort = $request->has('sort') && $request->sort != ''
                    ? explode('|', $request->sort)
                    : null;

        if ($reqsort) {
            // recupera el valor de 'sort' en array ['campo' => 'nombre', 'direccion' => 'desc']
            $orden = ['campo' => $reqsort[0], 'direccion' => $reqsort[1]];

            $registros = Lider::orderBy($orden['campo'], $orden['direccion'])->paginate(20);
        } else {
            // sin argumento 'sort'
            $registros = Lider::orderBy('nombre')->get();
        }

        return response()->json($registros);

    }

}
