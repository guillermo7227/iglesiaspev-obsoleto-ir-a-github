<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\GuardarMedioRequest;
use App\Medio;

class AMedioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sitio.admin.medios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sitio.admin.medios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GuardarMedioRequest $request)
    {
        $datos = $request->validated();

        $datos['url'] = \Utils::getMedioRawURL($datos['url']);

        $registro = Medio::create($datos);

        \Alert::toast('El registro fue creado exitosamente.', 'success');

        return redirect()->route('sermones.medios.show', $registro->slug);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $slug)
    {
        $medio = Medio::where('slug', $slug)
                      ->firstOrFail();

        return view('sitio.admin.medios.edit', compact('medio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GuardarMedioRequest $request, string $slug)
    {
        $datos = $request->validated();

        $datos['url'] = \Utils::getMedioRawURL($datos['url']);

        $registro = Medio::where('slug', $slug)->firstOrFail();

        $registro->fill($datos);

        $registro->save();

        \Alert::toast('El registro fue actualizado exitosamente.', 'success');

        return redirect()->route('sermones.medios.show', $registro->slug);

    }


    public function apiDestroy(string $slug)
    {
        $registro = Medio::where('slug', $slug)->firstOrFail();

        $registro->delete();

        \Alert::toast('El registro fue eliminado exitosamente.', 'success');

        return response()->json([
            'exito' => 'Se eliminó el registro exitosamente.'
        ]);
    }



    public function apiTodos(Request $request)
    {
        $registros = null;

        // toma el valor del argumento 'sort', orden
        $reqsort = $request->has('sort') && $request->sort != '' ? explode('|', $request->sort) : null;

        if ($reqsort) {
            // recupera el valor de 'sort' en array ['campo' => 'nombre', 'direccion' => 'desc']
            $orden = ['campo' => $reqsort[0], 'direccion' => $reqsort[1]];

            $registros = Medio::orderBy($orden['campo'], $orden['direccion'])->paginate(20);
        } else {
            // sin argumento 'sort'
            $registros = Medio::orderBy('descripcion')->get();
        }

        return response()->json($registros);

    }

}
