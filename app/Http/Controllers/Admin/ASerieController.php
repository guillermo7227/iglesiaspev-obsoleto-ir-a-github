<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sermon;
use App\Serie;
use App\Medio;
use App\Lider;
use App\Http\Requests\GuardarSermonRequest;
use App\Http\Requests\GuardarSerieRequest;

class ASerieController extends Controller
{
    public function index()
    {

        return view('sitio.admin.series.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $slug)
    {
        $serie = Serie::where('slug', $slug)
                      ->firstOrFail();

        return view('sitio.admin.series.edit', compact(
            'serie',
        ));
    }

    public function create()
    {
        return view('sitio.admin.series.create');
    }


    public function store(GuardarSerieRequest $request)
    {
        $datos = $request->validated();

        $registro = Serie::create($datos);

        \Alert::toast('El registro fue creado exitosamente.', 'success');

        return redirect()->route('sermones.series.show', $registro->slug);
    }

    public function update(GuardarSerieRequest $request, string $slug)
    {
        $datos = $request->validated();

        $registro = Serie::where('slug', $slug)->firstOrFail();

        $registro->fill($datos);

        $registro->save();

        \Alert::toast('El registro fue actualizado exitosamente.', 'success');

        return redirect()->route('sermones.series.show', $registro->slug);
    }


    public function apiDestroy(string $slug)
    {
        $registro = Serie::where('slug', $slug)->firstOrFail();

        $registro->delete();

        \Alert::toast('El registro fue eliminado exitosamente.', 'success');

        return response()->json([
            'exito' => 'Se eliminó el registro exitosamente.'
        ]);
    }



    public function apiTodos(Request $request)
    {
        $registros = null;

        // toma el valor del argumento 'sort', orden
        $reqsort = $request->has('sort') && $request->sort != '' ? explode('|', $request->sort) : null;

        if ($reqsort) {
            // recupera el valor de 'sort' en array ['campo' => 'nombre', 'direccion' => 'desc']
            $orden = ['campo' => $reqsort[0], 'direccion' => $reqsort[1]];

            $registros = Serie::orderBy($orden['campo'], $orden['direccion'])->paginate(20);
        } else {
            // sin argumento 'sort'
            $registros = Serie::orderBy('titulo')->get();
        }

        return response()->json($registros);

    }

}
