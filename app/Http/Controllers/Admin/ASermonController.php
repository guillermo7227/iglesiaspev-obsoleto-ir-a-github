<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sermon;
use App\Serie;
use App\Medio;
use App\Lider;
use App\Http\Requests\GuardarSermonRequest;

class ASermonController extends Controller
{
    public function index()
    {

        return view('sitio.admin.sermones.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $slug)
    {
        $sermon = Sermon::with(['lider', 'medio', 'serie'])
                        ->where('slug', $slug)
                        ->firstOrFail();

        $series = Serie::orderBy('titulo')->get();
        $lideres = Lider::orderBy('nombre')->get();
        $medios = Medio::orderBy('descripcion')->get();

        return view('sitio.admin.sermones.edit', compact(
            'sermon',
            'series',
            'lideres',
            'medios',
        ));
    }

    public function create()
    {
        $series = Serie::all();
        $lideres = Lider::all();
        $medios = Medio::orderByDesc('created_at')->get();

        return view('sitio.admin.sermones.create', compact(
            'series',
            'lideres',
            'medios',
        ));
    }


    public function store(GuardarSermonRequest $request)
    {
        $datos = $request->validated();

        $registro = Sermon::create($datos);

        \Alert::toast('El registro fue creado exitosamente.', 'success');

        return redirect()->route('sermones.sermones.show', $registro->slug);
    }


    public function update(GuardarSermonRequest $request, string $slug)
    {
        $datos = $request->validated();

        $registro = Sermon::where('slug', $slug)->firstOrFail();

        $registro->fill($datos);

        $registro->save();

        \Alert::toast('El registro fue actualizado exitosamente.', 'success');

        return redirect()->route('sermones.sermones.show', $registro->slug);
    }


    public function apiDestroy(string $slug)
    {
        $registro = Sermon::where('slug', $slug)->firstOrFail();

        $registro->delete();

        \Alert::toast('El registro fue eliminado exitosamente.', 'success');

        return response()->json([
            'exito' => 'Se eliminó el registro exitosamente.'
        ]);
    }



    public function apiTodos(Request $request)
    {
        $registros = null;

        // toma el valor del argumento 'sort', orden
        $reqsort = $request->has('sort') && $request->sort != '' ? explode('|', $request->sort) : null;

        if ($reqsort) {
            // recupera el valor de 'sort' en array ['campo' => 'nombre', 'direccion' => 'desc']
            $orden = ['campo' => $reqsort[0], 'direccion' => $reqsort[1]];

            // si se ordena por nombre de lider, que es subrelationship
            if ($orden['campo'] == 'lider.nombre') {
                $registros = Sermon::with('lider')
                                   ->join('lideres', 'sermones.lider_id', '=', 'lideres.id')
                                   ->orderBy('lideres.nombre', $orden['direccion'])
                                   ->paginate(15);
            } else {
                $registros = Sermon::with('lider')
                                   ->orderBy($orden['campo'], $orden['direccion'])
                                   ->paginate(15);
            }
        } else {
            // sin argumento 'sort'
            $registros = Sermon::orderBy('titulo')->get();
        }

        return response()->json($registros);

    }

}
