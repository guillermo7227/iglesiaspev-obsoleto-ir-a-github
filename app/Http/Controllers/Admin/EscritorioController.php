<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EscritorioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sitio.admin.index');
    }

    /**
     * Activa la transmision en vivo en la página de inicio.
     *
     * @return \Illuminate\Http\Response
     */

    public function activarTransmision(\App\LlaveValor $llaveValor)
    {
        $estaTransmitiendo = $llaveValor::getEstaTransmitiendo();

        if ($estaTransmitiendo) {
            $llaveValor::updateOrCreate([
                'llave' => 'transmisionFin'
            ], [
                'valor' => now()->toString()
            ]);

            $msg = '';
            \Alert::toast('Se desactivó la transmisión exitosamente', 'success');
        } else {

            $channelId = config('services.youtube.channel_id');
            // $channelId = 'UCq_Zq5kSQ2DM3REWA2lKejw'; //radio eternidad
            // $channelId = 'UCLAYBAW9L3rPnTjPwdAqaxQ'; //guilleagudelo
            $api_key = config('services.youtube.api_key');

            $youtubeLive = new \App\Helpers\EmbedYoutubeLiveStreaming($channelId, $api_key);

            if ($youtubeLive->isLive) {
                $llaveValor::updateOrCreate([
                    'llave' => 'transmisionCodigo'
                ], [
                    'valor' => $youtubeLive->embed_code
                ]);

                $llaveValor::updateOrCreate([
                    'llave' => 'transmisionTitulo'
                ], [
                    'valor' => $youtubeLive->live_video_title
                ]);

                $llaveValor::updateOrCreate([
                    'llave' => 'transmisionFin'
                ], [
                    'valor' => now()->addMinutes(60)->toString()
                ]);

                \Alert::toast('Se activó la transmisión exitosamente', 'success');
            } else {
                \Alert::toast('El canal no está transmitiendo en este momento', 'error');
            }
        }

        return redirect()->route('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
