<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sermon;

class LibroController extends Controller
{
    public function index()
    {

        $sermones = Sermon::orderByDesc('fecha')->get();

        $libros = [];

        foreach($sermones as $sermon) {
            if (!in_array($sermon->libro, $libros)) {
                array_push($libros, $sermon->libro);
            }
        }

        $libros = collect($libros)->sort();

        return view('sitio.sermones.libros.index', compact(
            'libros',
            'sermones',
        ));
    }

    public function show(string $libro)
    {
        $libro = ucfirst($libro);

        $sermones = Sermon::ordenarPorLibroYVersiculos()
                          ->where('libro', $libro);

        $sermones = \Utils::paginateCollection($sermones, 20);


        return view('sitio.sermones.libros.show', compact(
            'sermones',
            'libro'
        ));
    }

}
