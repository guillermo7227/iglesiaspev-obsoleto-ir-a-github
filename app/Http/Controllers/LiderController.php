<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lider;

class LiderController extends Controller
{
    public function index()
    {
        $lideres = Lider::with('sermones')
                        ->get();

        return view('sitio.sermones.lideres.index', compact('lideres'));
    }

    public function show(string $slug)
    {
        $lider = Lider::with(['sermones', 'sermones.serie'])
                      ->where('slug', $slug)
                      ->firstOrFail();

        return view('sitio.sermones.lideres.show', compact(
            'lider',
        ));
    }
}
