<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medio;

class MedioController extends Controller
{

    public function show(string $slug)
    {
        $medio = Medio::with('sermon')
                      ->where('slug', $slug)
                      ->firstOrFail();

        return view('sitio.sermones.medios.show', compact(
            'medio',
        ));
    }

}
