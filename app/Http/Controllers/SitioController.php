<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lider;
use App\Testimonio;
use App\Sermon;
// use Corcel\Model\Post;
use App\Mail\Contacto;
use App\Http\Requests\GuardarSuscriptor;
use App\Suscriptor;
// use RealRashid\SweetAlert\Facades\Alert;
class SitioController extends Controller
{
// Use Alert;

    public function inicio(\App\LlaveValor $llaveValor)
    {

        $sermones = Sermon::with('lider', 'serie', 'medio')
            ->orderByDesc('fecha')
            ->take(6)->get();

        // Trae las dos entradas mas recientes del blog
        // $resultadosWP = Post::with('author', 'thumbnail')
        //     ->newest()
        //     ->type('post')
        //     ->published()
        //     ->take(4)->get();

        $resultadosWP = [];
        $entradas = [];

        /*
        foreach($resultadosWP as $res) {
            $entradas[$res->ID] = [
                'titulo' => $res->post_title,
                'autor' => $res->author->display_name,
                'fecha' => $res->post_date,
                'imagen' => $res->thumbnail ? $res->thumbnail->attachment->guid : null,
                'link' => $res->guid,
                'resumen' => $res->excerpt,
            ];
        }
        */

        $wp_entradas = json_decode(file_get_contents('https://blog.supalabraesverdad.org/wp-json/wp/v2/posts?filter[posts_per_page]=4'), true);

        foreach($wp_entradas as $res) {
            $img_json = json_decode(file_get_contents($res['_links']['wp:featuredmedia'][0]['href']), true);
            $entradas[$res['id']] = [
                'titulo' => $res['title']['rendered'],
                'autor' => 'ISPEV',
                'fecha' => $res['date'],
                'imagen' => $img_json['source_url'],
                'link' => $res['link'],
                'resumen' => $res['excerpt']['rendered'],
            ];
        }

        $transmision = (object)['codigo' => null, 'titulo' => null];
        $transmision->transmitiendo = $llaveValor::getEstaTransmitiendo();

        if ($transmision->transmitiendo) {
            $transmision->codigo = $llaveValor::find('transmisionCodigo')->valor;
            $transmision->titulo = $llaveValor::find('transmisionTitulo')->valor;
        }

        return view('inicio', compact(
            'entradas',
            'sermones',
            'transmision',
        ));

    }

    public function ministerios()
    {
        return view('sitio.paginas.ministerios');
    }

    public function loQueEnsenamos()
    {
        return view('sitio.paginas.lo_que_ensenamos');
    }

    public function lideres()
    {

        $lideres = Lider::all();

        return view('sitio.paginas.lideres', compact('lideres'));
    }

    public function testimonios()
    {

        $testimonios = Testimonio::all();

        return view('sitio.paginas.testimonios', compact('testimonios'));
    }

    public function contacto()
    {

        return view('sitio.paginas.contacto');
    }

    public function mensajeContacto(Request $request)
    {
        \Mail::to(EMAIL_CONTACTO)
            ->send(new Contacto($request->input()));

        \Alert::toast('Su mensaje fue enviado exitosamente. Gracias por contactarnos. Pronto nos comunicaremos con usted', 'success');

        return redirect()->route('sitio.contacto');
    }


    public function suscribirseBoletin(GuardarSuscriptor $request)
    {
        $datos = $request->validated();

        try {
            $registro = Suscriptor::create($datos);
        } catch (\Illuminate\Database\QueryException $ex) {
            $errorCode = $ex->errorInfo[1];
            if($errorCode == 1062){
                return 'Usted ya se encuentra suscrito a nuestro boletín.';
            }
        }

        \Alert::toast('Se ha suscrito satisfactoriamente al boletín de Su Palabra es Verdad.', 'success');

        return redirect()->route('sitio.inicio');
    }

    public function desuscribirseBoletin(Request $request)
    {
        $registro = Suscriptor::where('email', $request->email)->firstOrFail();

        $registro->delete();

        return response("El suscriptor {$registro->email} ha sido borrado de nuestra base de datos. Si desea suscribirse nuevamente a nuestro boletín, visite www.iglesiasupalabraesverdad.org y busque la opción de 'Suscribirse al boletín.'");
    }

    public function test()
    {
        // $suscriptores = Suscriptor::all();
        //
        // $sermones = Sermon::with('lider', 'serie', 'medio')
        //     ->take(2)
        //     ->get();
        //
        // // Trae las dos entradas mas recientes del blog
        // $resultadosWP = Post::with('author', 'thumbnail')
        //     ->newest()
        //     ->type('post')
        //     ->published()
        //     ->take(2)->get();
        //
        // $entradas = [];
        //
        // foreach($resultadosWP as $res) {
        //     $entradas[$res->ID] = [
        //         'titulo' => $res->post_title,
        //         'autor' => $res->author->display_name,
        //         'fecha' => $res->post_date,
        //         'imagen' => $res->thumbnail->attachment->guid,
        //         'link' => $res->guid,
        //         'resumen' => $res->excerpt,
        //     ];
        // }
        // \Mail::to('guille@guille.cmo')->send(new \App\Mail\BoletinMail($suscriptores[0], $sermones, $entradas));
        // return new \App\Mail\BoletinMail($suscriptores[0], $sermones, $entradas);
    }
}
