<?php

namespace App\Http\Middleware;

use Closure;

class Abortar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return response('No puede acceder a este recurso', 402);
        // return $next($request);
    }
}
