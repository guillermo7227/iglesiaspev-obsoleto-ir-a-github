<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        // TODO: Quitar en deploy
        // previene el mensaje de 'unauthenticated' cuand requests Ajax
        // Este método no debería ser necesario porque el origen para las
        // request Ajax seria el mismo, 'iglesiasupalabraesverdad.org'
        $esCors = $request->header('sec-fetch-mode') == 'cors';
        $esVallenateca = $request->header('host') == 'tests.vallenateca.com';
        $esAjax = strpos($request->header('accept'), 'pplication/json');

        if ($esCors && $esVallenateca && $esAjax) {
            return $next($request);
        }

        $this->authenticate($request, $guards);

        return $next($request);
    }

}
