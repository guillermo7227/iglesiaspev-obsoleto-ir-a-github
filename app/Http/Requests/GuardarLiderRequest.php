<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarLiderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'posicion' => 'required|in:' . implode(',', POSICIONES),
            // 'contacto' => ['required', 'regex:/^(?=.*instagram:)(?=.*facebook:)(?=.*email:)/'],
            'contacto' => ['required', 'regex:/^(?=.*email:)/'],
            'imagen' => 'nullable|image',
            'bio' => 'nullable',
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'contacto.regex' => 'Debe ingresar el correo electrónico',
        ];
    }

}
