<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarSermonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required',
            'fecha' => "required|date|before_or_equal:today",
            'libro' => 'required|in:' . implode(',', LIBROS),
            'versiculos' => 'required',
            'descripcion' => 'required',
            'medio_id' => 'required|exists:medios,id',
            'lider_id' => 'required|exists:lideres,id',
            'serie_id' => 'required|exists:series,id',
        ];
    }
}
