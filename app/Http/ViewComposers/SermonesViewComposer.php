<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Serie;
use App\Sermon;
use App\Lider;

class SermonesViewComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $sermonesRecientes = Sermon::orderByDesc('created_at')
                                   ->take(4)
                                   ->get();

        $seriesRecientesIds = Sermon::orderByDesc('fecha')
            ->pluck('serie_id')
            ->unique()
            ->take(4)
            ->toArray();

        $seriesRecientes = array_map(function($serieId) {
            return Serie::find($serieId);
        }, array_values($seriesRecientesIds));

        $lideresCreativos = Lider::creativos()
                                 ->take(4);

        $cuentaLibros = Sermon::cuentaPorLibro()
                              ->orderByDesc('cuenta')
                              ->take(4)
                              ->get();

        $view->with('sermonesRecientes', $sermonesRecientes);
        $view->with('seriesRecientes', $seriesRecientes);
        $view->with('lideresCreativos', $lideresCreativos);
        $view->with('cuentaLibros', $cuentaLibros);
    }
}
