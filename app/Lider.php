<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Serie;
use App\Sermon;
use Cviebrock\EloquentSluggable\Sluggable;

class Lider extends Model
{

    use Sluggable;

    protected $table = 'lideres';

    protected $fillable = [
        'nombre',
        'posicion',
        'contacto',
        'bio',
    ];

    protected $casts = [
        'contacto' => 'array',
    ];

    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function getContactoArrAttribute()
    {
        // $string = str_replace(' ', '', $this->contacto);

        $redes = explode(' ', $this->contacto);

        $array = [];

        foreach($redes as $red) {
            $redArr = explode(':', $red);
            $array["{$redArr[0]}"] = $redArr[1];
        }

        return $array;
    }

    public function sermones()
    {
        return $this->hasMany(Sermon::class);
    }

    public function getSeries()
    {
        $seriesAll = Serie::with('sermones')->get();

        $series = $seriesAll->filter(function($serie) {
            foreach($serie->sermones as $sermon) {
                if ($sermon->lider_id == $this->id) {
                    return true;
                }
            }
            return false;
        });

        return $series;
    }

    // lideres con mas sermones
    public static function creativos()
    {
        $lideres = static::with('sermones')->get()->map(function($lider) {
            $lider->cuenta = count($lider->sermones);
            return $lider;
        });

        return $lideres->sortByDesc('cuenta');
    }
}
