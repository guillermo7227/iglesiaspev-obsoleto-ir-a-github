<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LlaveValor extends Model
{
    protected $table = 'pares_llave_valor';
    public $primaryKey = 'llave';
    public $incrementing = false;
    protected $fillable = ['llave', 'valor'];


    public static function getEstaTransmitiendo()
    {
        $registro = static::find('transmisionFin');
        if (is_null($registro)) return false;

        $fechaFin = new \Carbon\Carbon($registro->valor);

        return $fechaFin > now();
    }
}
