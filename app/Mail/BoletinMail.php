<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoletinMail extends Mailable
{
    use Queueable, SerializesModels;

    public $suscriptor;
    public $sermones;
    public $entradas;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($suscriptor, $sermones, $entradas)
    {
        $this->suscriptor = $suscriptor;
        $this->sermones = $sermones;
        $this->entradas = $entradas;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Boletín Iglesia Su palabra es Verdad')
                    ->replyTo('noresponder@iglesiasupalabraesverdad.org')
                    ->view('mails.boletin');
    }
}
