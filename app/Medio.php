<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Medio extends Model
{

    use Sluggable;

    protected $table = 'medios';

    protected $fillable = [
        'tipo',
        'descripcion',
        'url',
    ];

    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'descripcion'
            ]
        ];
    }

    public function sermon()
    {
        return $this->hasOne(Sermon::class);
    }
}
