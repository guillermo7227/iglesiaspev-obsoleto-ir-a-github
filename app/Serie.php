<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sermon;
use Cviebrock\EloquentSluggable\Sluggable;

class Serie extends Model
{

    use Sluggable;

    protected $table = 'series';

    protected $fillable = [
        'titulo',
        'descripcion',
    ];


    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    public function sermones()
    {
        return $this->hasMany(Sermon::class);
    }

    public function getLibros()
    {
        $sermones = $this->sermones;

        $libros = [];

        foreach($sermones as $sermon) {
            if (!in_array($sermon->libro, $libros)) {
                array_push($libros, $sermon->libro);
            }
        }

        return $libros;
    }

    public function getLideres()
    {
        $lideresAll = Lider::with('sermones')->get();

        $lideres = $lideresAll->filter(function($lider) {
            foreach ($lider->sermones as $sermon) {
                if ($sermon->serie_id == $this->id) {
                    return true;
                }
            }
            return false;
        });

        return $lideres;
    }
}
