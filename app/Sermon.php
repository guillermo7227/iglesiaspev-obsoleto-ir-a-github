<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lider;
use App\Medio;
use App\Serie;
use Cviebrock\EloquentSluggable\Sluggable;

class Sermon extends Model
{
    use Sluggable;

    protected $table = 'sermones';

    protected $fillable = [
        'titulo',
        'fecha',
        'libro',
        'versiculos',
        'descripcion',
        'medio_id',
        'lider_id',
        'serie_id',
    ];

    protected $dates = ['fecha'];

    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }


    public function lider()
    {
        return $this->hasOne(Lider::class, 'id', 'lider_id');
    }

    public function serie()
    {
        return $this->hasOne(Serie::class, 'id', 'serie_id');
    }

    public function medio()
    {
        return $this->hasOne(Medio::class, 'id', 'medio_id');
    }

    public function getPasajeAttribute()
    {
        return $this->libro .' '. $this->versiculos;
    }

    public function getFechaFormateadaAttribute()
    {
        return $this->fecha->formatLocalized('%e %b %Y');
    }

    // cuenta de sermones agrupados por libro
    public static function cuentaPorLibro()
    {
        $agrupados = \DB::table('sermones')
                        ->select(\DB::raw('libro, count(*) as cuenta'))
                        ->groupBy('libro');

        return $agrupados;
    }

    // sermones ordenados por libro y versiculos
    public static function ordenarPorLibroYVersiculos()
    {
        $sermones = static::all()
            ->sort(function($a, $b) {
                if ($a->libro == $b->libro) {

                    $aCapitulo = \Utils::padCapitulo($a->versiculos);
                    $bCapitulo = \Utils::padCapitulo($b->versiculos);

                    if ($aCapitulo == $bCapitulo) {
                        $aVersiculo = \Utils::padVersiculo($a->versiculos);
                        $bVersiculo = \Utils::padVersiculo($b->versiculos);

                        return $aVersiculo - $bVersiculo;
                    }

                    return $aCapitulo - $bCapitulo;
                }

                return strcmp($a->libro, $b->libro);
            });

        return $sermones;
    }
}
