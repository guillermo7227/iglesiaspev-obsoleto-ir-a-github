<?php
/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Iglesia Cristiana Reformada en Valledupar Su Palabra es Verdad", // set false to total remove
            'titleBefore'  => true, // Put defaults.title before page title, like 'It's Over 9000! - Dashboard'
            'description'  => 'Iglesia Cristiana Reformada en Valledupar, Sana doctrina, 5 Solas, Doctrinas de la gracia, Jesus, Dios, Salvación, Evangelio', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => ['reformada', 'iglesia', 'cristiana', 'evangelica', 'doctrinas de la gracias', 'sana doctrina', 'valledupar', 'jesus', 'dios', 'evangelio', 'salvacion'],
            'canonical'    => null, // Set null for using Url::current(), set false to total remove
            'robots'       => false, // Set to 'all', 'none' or any combination of index/noindex and follow/nofollow
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null,
        ],

        'add_notranslate_class' => false,
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Iglesia Cristiana Reformada en Valledupar Su Palabra es Verdad', // set false to total remove
            'description' => 'Iglesia Cristiana Reformada en Valledupar, Sana doctrina, 5 Solas, Doctrinas de la gracia, Jesus, Dios, Salvación, Evangelio', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'website',
            'site_name'   => 'Iglesia Cristiana Reformada en Valledupar Su Palabra es Verdad',
            'images'      => ['https://supalabraesverdad.org/img/logotexto.png'],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
    'json-ld' => [
        /*
         * The default configurations to be used by the json-ld generator.
         */
        'defaults' => [
            'title'       => 'Iglesia Cristiana Reformada en Valledupar Su Palabra es Verdad', // set false to total remove
            'description' => 'Iglesia Cristiana Reformada en Valledupar, Sana doctrina, 5 Solas, Doctrinas de la gracia, Jesus, Dios, Salvación, Evangelio', // set false to total remove
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => 'WebPage',
            'images'      => ['https://supalabraesverdad.org/img/logotexto.png'],
        ],
    ],
];
