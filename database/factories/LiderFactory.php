<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lider;
use Faker\Generator as Faker;

$factory->define(Lider::class, function (Faker $faker) {
    return [
        'nombre' => $faker->firstName('male') . ' ' . $faker->lastName(),
        'posicion' => $faker->randomElement(POSICIONES),
        'contacto' => "instagram:{$faker->word()} facebook:{$faker->word()} email:{$faker->email()}",
        'imagen' => 'img/persona.jpg',
        'bio' => $faker->text(700),
    ];
});
