<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Medio;
use Faker\Generator as Faker;

$factory->define(Medio::class, function (Faker $faker) {
    $tipo = $faker->randomElement(TIPOS_MEDIO);
    switch ($tipo) {
    case 'Audio':
        $url = asset('archivos/audio/sermones/sermon1.mp3');
        break;
    case 'Video':
        $url = 'https://www.youtube.com/watch?v=nrYbeJLLmgY';
        break;
    case 'Documento':
        $url = asset('archivos/Lo que enseñamos.pdf');
        break;
    }
    return [
        'tipo' => $tipo,
        'url' => $url,
        'descripcion' => $faker->sentence(),
    ];
});


