<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Serie;
use Faker\Generator as Faker;

$factory->define(Serie::class, function (Faker $faker) {
    return [
        'titulo' => ucfirst($faker->sentence(4)),
        'descripcion' => $faker->text(200),
    ];
});
