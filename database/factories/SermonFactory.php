<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sermon;
use Faker\Generator as Faker;
use App\Serie;
use App\Lider;
use App\Medio;

$factory->define(Sermon::class, function (Faker $faker) {
    $medios = Medio::all()->toArray();
    $lideres = Lider::all()->toArray();
    $series = Serie::all()->toArray();
    return [
        'titulo' => ucfirst($faker->sentence(4)),
        'fecha' => $faker->date(),
        'libro' => $faker->randomElement(LIBROS),
        'versiculos' => rand(1,150) . ':' . rand(1,20) . '-' . rand(20,40),
        'descripcion' => $faker->text(200),
        'medio_id' => $faker->randomElement($medios)['id'],
        'lider_id' => $faker->randomElement($lideres)['id'],
        'serie_id' => $faker->randomElement($series)['id'],
    ];
});
