<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Suscriptor;

$factory->define(Suscriptor::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name(),
        'email' => $faker->email(),
    ];
});
