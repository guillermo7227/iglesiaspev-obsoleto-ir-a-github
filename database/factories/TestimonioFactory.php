<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Testimonio;

$factory->define(Testimonio::class, function (Faker $faker) {
    return [
        'nombre' => ucfirst($faker->firstName() .' '. $faker->lastName()),
        'descripcion' => $faker->text(300),
        'video' => 'https://youtu.be/VssT5zR79sU',
    ];
});
