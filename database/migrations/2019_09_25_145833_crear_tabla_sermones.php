<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaSermones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sermones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->date('fecha');
            $table->enum('libro', LIBROS);
            $table->string('versiculos');
            $table->text('descripcion');
            $table->string('slug')->index();
            $table->bigInteger('medio_id')->unsigned();
            $table->bigInteger('lider_id')->unsigned();
            $table->bigInteger('serie_id')->unsigned();
            $table->timestamps();

            $table->foreign('medio_id')->references('id')->on('medios');
            $table->foreign('lider_id')->references('id')->on('lideres');
            $table->foreign('serie_id')->references('id')->on('series');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sermones');
    }
}
