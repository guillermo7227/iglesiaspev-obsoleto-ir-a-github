<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(LiderSeeder::class);
        $this->call(MedioSeeder::class);
        $this->call(SerieSeeder::class);
        $this->call(SermonSeeder::class);
        $this->call(TestimonioSeeder::class);
    }
}
