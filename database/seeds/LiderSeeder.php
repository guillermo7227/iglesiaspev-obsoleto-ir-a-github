<?php

use Illuminate\Database\Seeder;
use App\Lider;

class LiderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Lider::class, 5)->create();
    }
}
