<?php

use Illuminate\Database\Seeder;
use App\Suscriptor;

class SuscriptorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Suscriptor::class, 10)->create();
    }
}
