<?php

use Illuminate\Database\Seeder;
use App\Testimonio;

class TestimonioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Testimonio::class, 5)->create();
    }
}
