#!/bin/bash
set -e 

printf "Corriendo comandos post deploy\n\n"

printf ">php artisan ziggy:generate\n"
php artisan ziggy:generate
printf "Hecho.\n\n"

printf ">cp resources/assets/js/ziggy.js resources/js/\n"
cp resources/assets/js/ziggy.js resources/js/
printf "Hecho.\n\n"

printf ">npm install\n"
npm install
printf "Hecho.\n\n"

printf ">npm run prod\n"
npm run prod
printf "Hecho.\n\n"
