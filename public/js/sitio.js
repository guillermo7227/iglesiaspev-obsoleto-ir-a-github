$(document).ready(function() {

  //evento para mostrar el boton 'ir arriba'
  let btnArriba = $('#irArriba');

  $(window).scroll(function() {
    if ($(window).scrollTop() > document.documentElement.clientHeight) {
      $(btnArriba).css('visibility', 'visible')
    } else {
      $(btnArriba).css('visibility', 'hidden')
    }
  });

  btnArriba.on('click', function(e) {
    e.preventDefault()
    $('html, body').animate({scrollTop:0}, '300');
  });

  // handle links with @href started with '#' only
  $(document).on('click', 'a[href^="#"]', function(e) {
      // target element id
      var id = $(this).attr('id');

      // target element
      var $id = $(id);
      if ($id.length === 0) {
          return;
      }

      // prevent standard hash navigation (avoid blinking in IE)
      e.preventDefault();

      // top position relative to the document
      var pos = $id.offset().top;

      // animated top scrolling
      $('body, html').animate({scrollTop: pos});
  });
});

