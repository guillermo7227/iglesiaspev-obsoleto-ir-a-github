## Sitio Web de la iglesia Su Palabra es Verdad

Puede visitar el sitio en [Iglesia Su Palabra es Verdad](https://tests.vallenateca.com/iglesia)

Este sitio está hecho en Laravel 6, Bootstrap 4 y Vue 2.6.

Copyright 2019 - Guillermo Agudelo
[guille.agudelo@gmail.com](mailto:guille.agudelo@gmail.com)
