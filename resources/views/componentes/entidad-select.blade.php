
<div class="form-group">
  <label>{{ $etiqueta }}:</label>
  <div class="row">
    <div class="col">
      <select id="{{ $campo }}"
              class="selectpicker form-control"
              data-live-search="true"
              title="Seleccione una opción"
              required
              name="{{ $campo }}">
        @foreach($entidades as $entidad)
          <option value="{{ $entidad->id }}">
            {{ $entidad->$campoAMostrar }}
            @if (isset($campoAMostrarSegundario))
              ({{ $entidad->$campoAMostrarSegundario }})
            @endif
          </option>
        @endforeach
      </select>
      @if ($errors->has($campo))
        <div class="alert alert-danger">
          {{ $errors->first($campo) }}
        </div>
      @endif
    </div>

    <div class="col-auto px-1">
      <a class="btn btn-outline-primary"
         href="{{ route("admin.${ruta}.create") }}"
         href="#"
         title="Crear nuevo/a {{ str_replace('_id', '', $campo) }}"
         target="_blank">
        <i class="fa fa-plus"></i>
      </a>
    </div>

    <div class="col-auto px-1">
      <button class="btn btn-outline-secondary"
              title="Refrescar la lista"
              type="button"
              onclick="refrescarLista('{{ $ruta }}',
                                      '{{ $campo }}',
                                      '{{ $campoAMostrar }}',
                                      '{{ $campoAMostrarSegundario ?? null }}')">
        <i class="fa fa-sync-alt"></i>
      </button>
    </div>

  </div>
</div>
@push('script')
  <script>
    function refrescarLista(ruta, campo, campoAMostrar, campoAMostrarSegundario) {
      $.ajax({
        type: 'GET',
        url: `/admin/${ruta}/todos`,
        success: function(data) {
          let select = document.getElementById(campo);

          // borra los 'options' del select
          while (select.length > 0) {
            select.options.remove(0);
          }

          // llena el select con 'options' de la bd
          for (item of data) {
            let option = document.createElement('option');
            option.value = item.id;
            option.text = campoAMostrarSegundario
                          ? `${item[campoAMostrar]} (${item[campoAMostrarSegundario]})`
                          : item[campoAMostrar];
            select.options.add(option);
          }

          $(select).selectpicker('refresh');
          $.toast({
            title: 'Se ha actualizado la lista',
            type: 'info',
            delay: 5000
          });
        },
        error: function(data){
          bootbox.alert(`Error ${data.status} (${data.statusText}) al cargar el listado.`);
        }
      });

    }
  </script>
@endpush
