<div class="media entrada">
  <div class="imagen alig-self-start mr-2 border"
       style="background-image: url('{{ $entrada['imagen'] }}')">
  </div>

  <div class="media-body m-1 d-flex align-items-start flex-column">

    <div class="mt-0 entrada-titulo">
      <a href="{{ $entrada['link'] }}">
        {{ $entrada['titulo'] }}
      </a>
    </div>

    <div class="entrada-resumen mt-2">
      <p class="mb-0" style="font-size: 0.9rem">
        <small>{!! $entrada['resumen'] !!}...</small>
      </p>
    </div>

    <div class="entrada-pie mt-2">
      <p class="font-italic text-muted mb-0" style="font-size:0.8rem">
        Fecha: {{ date("d-m-Y", strtotime($entrada['fecha'])) }} <br>
        Por: {{ $entrada['autor'] }}
      </p>
    </div>
  </div>
</div>
