<h3 class="my-0">
  <a href="{{ route('sermones.sermones.show', $sermon->slug) }}" class="text-reset">
    {{ $sermon->titulo }}
  </a>
</h3>
<small>
  <p class="text-muted mb-0">
    @include('parciales.pasaje-span', [
      'libro' => $sermon->libro,
      'claseParcial' => 'text-reset',
    ])-
    @include('parciales.lider-span', [
      'lider' => $sermon->lider,
      'claseParcial' => 'text-reset',
    ])-
    {{ $sermon->fechaFormateada }}
  </p>

  <p class="text-muted">
    Serie:
    @include('parciales.serie-span', [
      'serie' => $sermon->serie,
      'claseParcial' => 'text-reset',
    ])
  </p>
</small>

@include('parciales.medio-display', [
  'medio' => $sermon->medio
])


