@extends('layout.master')
@section('titulo', 'Inicio')
@section('contenido')

  @include('sitio.inicio.heroe')

  @include('sitio.inicio.cuenta_atras')

  @include('sitio.inicio.transmision')

  @include('sitio.inicio.evangelio')

  @include('sitio.inicio.sermones')

  @include('sitio.inicio.entradas')

  @include('sitio.inicio.servicios')

@endsection

