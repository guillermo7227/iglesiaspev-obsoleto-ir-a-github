<style>

.btn-primary {
    color: #ebebeb !important;
}

.banner{
  background-image:url('{{ config("app.url") }}/img/fondo_hero.jpg');
  text-align: center;
  color: #f9f9f9;

  background-repeat: no-repeat;
  background-attachment: scroll;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

/* numeros del counter */
.number {
  background-color: #f8fafc !important;
}

.btn {
  font-weight: bold;
  padding: 5px 15px;
}

.btn {
  border-radius: 5rem;
}

.irArriba {
  border-radius: 50%;
  position: fixed;
  bottom: 15px;
  right: 15px;
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  visibility: hidden;
  z-index: 1000;
  text-decoration: none;
  border: solid 1px #d2925b;
  box-shadow: 0.1px 0.5px 2px #794d27;
}

.irArriba:hover {
  text-decoration: none;
}

.navbar {
    -webkit-transition: all 0.6s ease-out;
    -moz-transition: all 0.6s ease-out;
    -o-transition: all 0.6s ease-out;
    -ms-transition: all 0.6s ease-out;
    transition: all 0.6s ease-out;
    background: rgb(18, 18, 18); /* IE */
    background: rgba(18, 18, 18, 1); /* NON-IE */
}

footer {
    background: rgb(18, 18, 18); /* IE */
    background: rgba(18, 18, 18, 1); /* NON-IE */
    color: #9f9f9f;
    font-size: 1rem;
    padding: 1.5rem;
}

.hero {
  margin-top: -95px;
}

.entrada {
  max-height: 200px;
}

.entrada .imagen {
  width: 120px;
  height: 200px;
  background-size: cover;
  background-position: center;
}

.entrada .media-body {
  height: 190px;
}

.entrada-titulo,
.entrada-pie,
.entrada-resumen {
  overflow: hidden;
  overflow-text: ellipsis;
}

.entrada-titulo {
  font-size: 1.3rem;
  line-height: 27px;
  max-height: 54px;
}

.entrada-resumen {
  line-height: 21px;
  max-height: 84px;
}

.entrada-pie {
  line-height: 19px;
  max-height: 38px;
}

.card-title {
    font-size: 1.4rem;
}

.foto-lider {
  border: solid 4px #e3c034;
  /* border-top-style: solid; */
  /* border-right-style: solid; */
  /* border-bottom-style: solid; */
  /* border-left-style: solid; */
  border-radius: 5%;
  border-style: outset;
}

.video-responsive{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.video-responsive iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}

.text-claro {
  color: #ece3c3 !important;
}
.text-oscuro {
  color: #2A2C2B !important;
}

.sermones {
  background-repeat: no-repeat !important;
  background-size: cover !important;
  background-position: right top !important;
}

.img-fondo {
  background-repeat: no-repeat !important;
  background-position: center !important;
  background-size: cover !important;
}

.img-circulo {
  width: 90px;
  height: 90px;
}

.img-galeria {
  max-height: 120px;
}

.text-semimuted {
  color: #33373c !important;
}

.miniatura-video {
  width: 290px;
  height: 150px;
}
.miniatura-video::before {
  content: url('{{ asset("img/boton_play.png") }}');
  display: flex;
  align-items: center;
  justify-content: center;
  height: inherit;
}

.text-decoration-underline {
  text-decoration: underline !important;
}

.swal2-content,
.swal2-title {
  font-size: 1.5rem !important;
}

.seccion-lateral {
  font-size: 1rem;
}

.bullet-inside {
  list-style-position: inside;
}
.float-wp{
  position:fixed;
  width:50px;
  height:50px;
  bottom:15px;
  left:20px;
  background-color:#25d366;
  color:#FFF;
  border-radius:50px;
  text-align:center;
  font-size:30px;
  box-shadow: 2px 2px 3px #999;
  z-index:100;
}

.my-float-wp{
  margin-top:10px;
}
</style>
