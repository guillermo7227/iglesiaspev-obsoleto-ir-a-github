<footer>
<div class="container-fluid">
  <div class="row">

    <div class="col-md-6">
      <div class="row">
        <div class="col-md-4 text-center text-md-left">
          <p class="mb-2 text-light"><strong>ENLACES</strong></p>
          <ul class="p-0 bullet-inside">
            <li><a class="text-reset" href="{{ route('sitio.inicio') }}">Inicio</a></li>
            <li><a class="text-reset" href="{{ route('sitio.contacto') }}">Contacto</a></li>
          </ul>
        </div>

        <div class="col-md-4 text-center text-md-left">
          <p class="mb-2 text-light"><strong>RECURSOS</strong></p>
          <ul class="p-0 bullet-inside">
            <li><a class="text-reset" href="{{ route('sermones.sermones.index') }}">Sermones</a></li>
            {{--<li><a class="text-reset" href="{{ config('app.url') }}/blog">Blog</a></li>--}}
          </ul>
        </div>

        <div class="col-md-4 text-center text-md-left">
          <p class="mb-2 text-light"><strong>IGLESIA</strong></p>
          <ul class="p-0 bullet-inside">
            <li><a class="text-reset" href="{{ route('sitio.lo_que_ensenamos') }}">Lo que enseñamos</a></li>
            <!-- <li><a class="text-reset" href="{{ route('sitio.ministerios') }}">Ministerios</a></li> -->
            <li><a class="text-reset" href="{{ route('sitio.lideres') }}">Nuestros Líderes</a></li>
            <!-- <li><a class="text-reset" href="{{ route('sitio.testimonios') }}">Testimonios</a></li> -->
          </ul>
        </div>

      </div>
    </div>


<!-- d&#45;flex align&#45;items&#45;end flex&#45;column text&#45;right -->
    <div class="col-md-6 ">
      <div class="row mb-auto">
        <div class="col pr-md-5 text-md-right text-center">
          <ul class="list-inline">
            <li class="list-inline-item">
              <a href="https://www.facebook.com/Iglesia-Cristiana-Su-Palabra-es-Verdad-1532157290211246/"
                 target="_blank">
                <i class="fab fa-lg fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://youtube.com/channel/{{ config('services.youtube.channel_id') }}"
                target="_blank">
                <i class="fab fa-lg fa-youtube"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="mailto:contacto@supalabraesverdad.org"><i class="fa fa-lg fa-envelope"></i></a>
            </li>
          </ul>
        </div>
      </div>

      <div class="row">
        <div class="col pr-md-5 text-md-right text-center">
          Calle 25 # 17-76 Barrio Simón Bolívar<br>
          Valledupar, Colombia<br>
          (C) 2020 Iglesia Su Palabra es Verdad
        </div>
      </div>
    </div>


  </div>
</div>
</footer>
