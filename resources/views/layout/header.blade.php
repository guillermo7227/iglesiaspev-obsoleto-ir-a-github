<header>
<nav class="px-md-5 navbar navbar-expand-md navbar-dark ">
    <a class="navbar-brand" href="{{ config('app.url') }}">
      <img src="{{ asset('img/logotexto.png') }}" height="70" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="/">INICIO</span></a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            IGLESIA
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('sitio.lo_que_ensenamos') }}">LO QUE ENSEÑAMOS</a>
            <!-- <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('sitio.ministerios') }}">MINISTERIOS</a> -->
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('sitio.lideres') }}">NUESTROS LÍDERES</a>
            {{-- <div class="dropdown-divider"></div> --}}
            {{-- <a class="dropdown-item" href="{{ route('sitio.testimonios') }}">TESTIMONIOS</a> --}}
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            RECURSOS
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('sermones.sermones.index') }}">SERMONES</a>
            <div class="dropdown-divider"></div>
            {{--<a class="dropdown-item" href="{{ URL_BLOG }}">BLOG</a>--}}
            <a class="dropdown-item" href="{{ URL_ESTUDIOS }}">ESTUDIOS</a>
          </div>
        </li>


        <li class="nav-item">
          <a class="nav-link" href="{{ route('sitio.contacto') }}">CONTACTO</a>
        </li>

        @if (Auth::check())

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ Auth::user()->primerNombre }}
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <span class="dropdown-item font-weight-bold text-uppercase px-2">{{ Auth::user()->name }}</span>
              <div class="dropdown-divider"></div>

              <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Perfil</a>
              <div class="dropdown-divider"></div>

              <a class="dropdown-item" href="{{ route('admin.index') }}">
                <i class="fa fa-tools"></i> Administración
              </a>
              <div class="dropdown-divider"></div>

              <a class="dropdown-item" href="{{ route('logout') }}"
                 onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out-alt"></i> {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </li>

        @else
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}" title="Acceder">
              <i class="fa fa-sign-in-alt"></i>
            </a>
          </li>
        @endif
      </ul>
    </div>
</nav>
</header>

<script>
  document.addEventListener('DOMContentLoaded', () => {
    $('.navbar-toggler').on('click', (ev) => {
      setTimeout(() => {
        const colapsado = $('.navbar-toggler')[0].classList.contains('collapsed');
        $('.navbar')[0].style.setProperty('background-color', colapsado
                                                              ? 'transparent'
                                                              : 'rgba(18, 18, 18, 1)','important');
      }, 100);
    })
  })
</script>
