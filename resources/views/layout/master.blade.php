<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta id="csrf-token" name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

        <!-- favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <!-- fin favicon -->

        <title>@yield('titulo') - Iglesia Cristiana Reformada en Valledupar Su Palabra es Verdad</title>

        {!! SEO::generate() !!}

        @if (config('app.env') == 'production')
          <!-- Google AdSende -->
          <script data-ad-client="ca-pub-9031563899394526" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        @endif

        <!-- styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-0c38nfCMzF8w8DBI+9nTWzApOpr1z0WuyswL4y6x/2ZTtmj/Ki5TedKeUcFusC/k" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/toast.min.css') }}">
      @include('layout.assets.master_css')
        @stack('style')

    </head>
    <body>
      @include('layout.header')

      <!-- boton para ir al inicio de la página que aparce en la parte inferior -->
      <a id="irArriba" class="irArriba bg-primary text-light" href="#">
        <i class="fa fa-arrow-up fa-lg"></i>
      </a>

      <div id="app">
        @yield('contenido')
      </div>

      <a href="https://api.whatsapp.com/send?phone=573024229912&text=Hola,%20quisiera%20contactar%20con%20ustedes..." class="float-wp" target="_blank" title="Contáctenos por Whatsapp">
        <i class="fab fa-whatsapp my-float-wp"></i>
      </a>

      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      @include('layout.footer')

      @stack('script-urgente')
      <script src="{{ asset('js/manifest.js') }}"></script>
      <script src="{{ asset('js/vendor.js') }}"></script>
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/bootbox.min.js') }}"></script>
      <script src="{{ asset('js/toast.min.js') }}"></script>
      @include('sweetalert::alert')
      <script src="{{ asset('js/sitio.js') }}"></script>
      @stack('script')
    </body>
</html>
