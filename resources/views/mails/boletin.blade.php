<h2>Hola {{ $suscriptor->nombre }},</h2>
<p>Aquí están las novedades de la Iglesia Su Palabra es Verdad.</p>

<h3>Sermones mas recientes</h3>

@foreach ($sermones as $sermon)
  <div class="item">
    <h4>{{ $sermon->titulo }}</h4>

    <p>
    {{ $sermon->descripcion }}
    </p>
    <p>
    <span class="text-muted">Pasaje:</span> {{ $sermon->pasaje }}
    <span class="text-muted">Serie:</span> {{ $sermon->serie->titulo }} <br>
    <span class="text-muted">Predicador:</span> {{ $sermon->lider->nombre }}
    <span class="text-muted">Fecha:</span> {{ $sermon->fechaFormateada }}
    </p>
    <a href="{{ route('sermones.sermones.show', $sermon->slug) }}"
       target="_blank">
      Ver sermon...
    </a>
  </div>

@endforeach

<hr>

<h3>Entradas del Blog mas recientes</h3>

@foreach ($entradas as $entrada)
  <div class="item">
    <h4>{{ $entrada['titulo'] }}</h4>

    <p>
      {{ $entrada['resumen'] }}
    </p>

    <p>
      <span class="text-muted">Autor:</span> {{ $entrada['autor'] }}
      <span class="text-muted">Fecha:</span> {{ date('d M Y', strtotime($entrada['fecha'])) }}
    </p>

    <a href="{{ $entrada['link'] }}"
       target="_blank">
      Ver entrada...
    </a>
  </div>

@endforeach


<p>
  Esperamos que estos recursos te edifiquen en tu vida cristiana.
</p>

<p>Cordialmente,</p>

<h3>
  Iglesia Cristiana Su Palabra es Verdad <br>
  <a href="{{{ route('sitio.inicio') }}}"
     target="_blank">
    www.iglesiasupalabraesverdad.org
  </a>
</h4>

<form action="{{ route('sitio.suscribirse_boletin') }}"
      method="POST"
      target="_blank">
  @method('delete')
  @csrf

  <input type="hidden" name="email" value="{{ $suscriptor->email }}">
  <p>
    <small class="text-muted">
      Si no desea seguir recibiendo este boletín, puede
      {{-- <a href="{{ route('sitio.desuscribirseBoletin') }}">desuscribirse.</a> --}}
      <input class="btn-desuscribirse" type="submit" value="desuscribirse">
    </small>
  </p>


</form>


<style>
  body {
    font-family: sans-serif;
    color: #292929;
  }

  .text-muted {
    color: gray;
  }

  .item {
    margin-bottom: 40px;
  }

  h4 {
    line-height: 0.1rem;
  }

  .btn-desuscribirse {
    background: transparent;
    border: none;
    cursor: pointer;
    color: gray;
    text-decoration: underline;
  }

</style>


