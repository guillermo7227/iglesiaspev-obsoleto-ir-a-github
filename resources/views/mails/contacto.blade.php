
<h5>
  Este es un mensaje recibido desde el formulario de contacto del sitio
  web de la Iglesia Cristiana Su Palabra es Verdad.
</h5>

<div class="h5">

  <p>
  Remitente:<br>
  <strong>{{ $datos['nombre'] }}</strong>
  (<strong class="text-muted">{{ $datos['email'] }}</strong>)
  </p>

  <p>
  Mensaje:<br>
  <strong>{{ $datos['mensaje'] }}</strong>
  </p>

</div>
