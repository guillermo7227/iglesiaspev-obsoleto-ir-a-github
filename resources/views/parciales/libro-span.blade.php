<span>
  <a href="{{ route('sermones.libros.show', $libro) }}" class="{{ $claseParcial ?? '' }}">
    {{ $libro }}
  </a>

  @if (isset($mostrarCuenta) && $mostrarCuenta == true)
    <small class="text-muted">
      ({{ $cuenta }} {{ $cuenta == 1 ? 'sermón' : 'sermones' }})
    </small>
  @endif
</span>
