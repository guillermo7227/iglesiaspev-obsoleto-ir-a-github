<span>
  <a href="{{ route('sermones.lideres.show', $lider->slug) }}"
     class="{{ $claseParcial ?? '' }}">
    @if (isset($mostrarIcono) && $mostrarIcono == true)
      <i class="far fa-xs fa-user"></i>
    @endif
    {{ $lider->nombre }}
  </a>
</span>
