
@switch($medio->tipo)
@case('Documento')
  <p>
    {{ $medio->tipo }}:
    <a href="{{ $medio->url }}" target="_blank">{{ $medio->url }}</a>
  </p>
  @break

@case('Audio')
  <p>
    <audio controls>
      <source src="{{ $medio->url }}" type="audio/mp3">
    </audio>
  </p>
  @break

@case('Video')
  <div class="video-responsive mb-3">
      <iframe id="ytvideo" width="480" height="270"
              src="https://www.youtube.com/embed/{{ substr($medio->url, -11) }}"
              frameborder="0" allow="accelerometer; encrypted-media ; gyroscope; picture-in-picture"
              allowfullscreen>
      </iframe>
  </div>
  @break
@endswitch

