<span>
  <a href="{{ route('sermones.series.show', $serie->slug) }}" class="{{ $claseParcial ?? '' }}">
    @if (isset($mostrarIcono) && $mostrarIcono == true)
      <i class="far fa-xs fa-copy"></i>
    @endif
    {{ $serie->titulo }}
  </a>
</span>
