<span>
  <a href="{{ route('sermones.sermones.show', $sermon->slug) }}" class="{{ $claseParcial ?? '' }}">
    @if (isset($mostrarIcono) && $mostrarIcono == true)
      <i class="far fa-xs fa-sticky-note"></i>
    @endif

    {{ $sermon->titulo }}
  </a>

  @if (isset($mostrarLider) && $mostrarLider == true)
    <small class="text-semimuted">
      {{ $sermon->lider->nombre }}.
    </small>
  @endif

  @if (isset($mostrarPasaje) && $mostrarPasaje == true)
    <small class="text-muted">
      {{ $sermon->pasaje }}.
    </small>
  @endif

  @if (isset($mostrarFecha) && $mostrarFecha == true)
    <small class="text-muted font-italic">
      ({{ $sermon->fechaFormateada }})
    </small>
  @endif
</span>
