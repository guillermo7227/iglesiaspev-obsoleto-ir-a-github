@extends('sitio.admin.layout.admin_master')
@push('style')
  <style>
    .btn-tarea {
      width: 100px;
      height: 90px;
      background: #dcdbd9;
      border: solid 1px #b9b8b8;
      border-radius: 0.2rem;
      box-shadow: 1px 1px 2px;
    }
    .btn-tarea:hover {
      background: #cecccc;
    }
    .texto-tarea {
      line-height: 1.2rem;
    }
  </style>
@endpush
@section('titulo', 'Escritorio de Administración')
@section('admin_contenido')

    <h1>Escritorio de Administración</h1>

    <p>

En el modulo de Administración puede crear, modificar y eliminar Sermones, Series, Líderes y Medios. Igualmente puede activar la transmisión en vivo que se mostrará en la página de inicio.

    </p>

    <h2>Tareas comunes</h2>

    <div class="d-flex justify-content-center">
        <div class="btn-tarea d-flex flex-column justify-content-center align-items-center m-2 p-2">
          <a href="{{ route('admin.sermones.create') }}" class="text-reset text-decoration-none">
            <div class="icono-tarea text-center">
              <i class="fa fa-sticky-note"></i>
            </div>

            <div class="texto-tarea text-center">
              Crear un sermón
            </div>
          </a>
        </div>

        <div class="btn-tarea d-flex flex-column justify-content-center align-items-center m-2 p-2">
          <a href="{{ route('admin.series.create') }}" class="text-reset text-decoration-none">
            <div class="icono-tarea text-center">
              <i class="fa fa-copy"></i>
            </div>

            <div class="texto-tarea text-center">
              Crear una serie
            </div>
          </a>
        </div>

        <div class="btn-tarea d-flex flex-column justify-content-center align-items-center m-2 p-2">
          <a href="{{ route('admin.lideres.create') }}" class="text-reset text-decoration-none">
            <div class="icono-tarea text-center">
              <i class="fa fa-user"></i>
            </div>

            <div class="texto-tarea text-center">
              Crear un líder
            </div>
          </a>
        </div>


        <div class="btn-tarea d-flex flex-column justify-content-center align-items-center m-2 p-2">
          <a href="{{ route('admin.medios.create') }}" class="text-reset text-decoration-none">
            <div class="icono-tarea text-center">
              <i class="fa fa-photo-video"></i>
            </div>

            <div class="texto-tarea text-center">
              Crear un medio
            </div>
          </a>
        </div>


        <div class="btn-tarea btn-info bg-info d-flex flex-column justify-content-center align-items-center m-2 p-2"
             title="La transmisión en vivo aparacerá en la página de inicio">
          <a href="{{ route('admin.activar-transmision') }}"
             class="text-reset text-decoration-none">
            <div class="icono-tarea text-center">
              <i class="fa fa-video"></i>
            </div>

            <div class="texto-tarea text-center">
              {{ App\LlaveValor::getEstaTransmitiendo() ? 'Desactivar' : 'Activar' }}
              transmisión en vivo
            </div>
          </a>
        </div>

    </div>

@endsection
