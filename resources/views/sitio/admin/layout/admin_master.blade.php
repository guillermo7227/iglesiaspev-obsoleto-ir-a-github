@extends('layout.master')
@section('contenido')

  <div class="container-fluid">

    <div class="row">

      <!-- Barra lateral -->
      @include('sitio.admin.layout.barra_lateral')


      <!-- Columna de sermones -->
      <div class="col-md-10 my-3">

        @yield('admin_contenido')

      </div>

    </div>

  </div>


@endsection
