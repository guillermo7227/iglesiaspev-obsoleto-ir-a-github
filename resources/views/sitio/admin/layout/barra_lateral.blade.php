@push('style')
<style>
  .admin-menu a {
    text-decoration: none;
  }
</style>
@endpush

<div class="col-md-2 admin-menu py-5 bg-secondary text-muted">

  <h4>MENÚ</h4>


  <div class="item-menu">
    <a href="{{ route('admin.index') }}">
      <i class="fa fa-sm fa-tachometer-alt"></i>
      Escritorio
    </a>
  </div>

  <hr class="my-1" style="border-color: rgba(111, 107, 107, 0.98);">


  <div class="item-menu">
    <a href="{{ route('admin.sermones.index') }}">
      <i class="fa fa-sm fa-sticky-note"></i>
      Sermones
    </a>
  </div>

  <hr class="my-1" style="border-color: rgba(111, 107, 107, 0.98);">


  <div class="item-menu">
    <a href="{{ route('admin.series.index') }}">
      <i class="fa fa-sm fa-copy"></i>
      Series
    </a>
  </div>

  <hr class="my-1" style="border-color: rgba(111, 107, 107, 0.98);">

  <div class="item-menu">
    <a href="{{ route('admin.lideres.index') }}">
      <i class="fa fa-sm fa-user"></i>
      Líderes
    </a>
  </div>

  <hr class="my-1" style="border-color: rgba(111, 107, 107, 0.98);">

  <div class="item-menu">
    <a href="{{ route('admin.medios.index') }}">
      <i class="fa fa-sm fa-photo-video"></i>
      Medios
    </a>
  </div>

  <hr class="my-1" style="border-color: rgba(111, 107, 107, 0.98);">





</div>

