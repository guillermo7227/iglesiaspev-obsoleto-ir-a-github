@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Crear Líder ')
@section('admin_contenido')
  <h1>Crear Líder</h1>

  <hr>

  <form action="{{ route('admin.lideres.store') }}"
        method="POST"
        enctype="multipart/form-data">
    @csrf

    <!-- row 1 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información general</h5>
      </div>

      <div class="col-md-8">

        <div class="form-group">
          <div class="form-group">
            <label>Nombre:</label>
            <input class="form-control"
                   type="text"
                   required
                   name="nombre"
                   value="{{ old('nombre') }}">
            @if ($errors->has('nombre'))
              <div class="alert alert-danger">
                {{ $errors->first('nombre') }}
              </div>
            @endif
          </div>
        </div>

        <div class="form-group">
          <div class="form-group">
            <label>Biografía:</label>
            <textarea class="form-control"
                      name="bio"
                      rows="5"
                      value="">{{ old('bio') }}</textarea>
            @if ($errors->has('bio'))
              <div class="alert alert-danger">
                {{ $errors->first('bio') }}
              </div>
            @endif
          </div>
        </div>

        <div class="form-group">
          <label>Posición:</label>
          <select id="posicion"
                  class="selectpicker form-control"
                  title="Seleccione una opción"
                  data-live-search="true"
                  required
                  name="posicion">
            @foreach(POSICIONES as $posicion)
              <option value="{{ $posicion }}">{{ $posicion }}</option>
            @endforeach
          </select>
          @if ($errors->has('posicion'))
            <div class="alert alert-danger">
              {{ $errors->first('posicion') }}
            </div>
          @endif
        </div>

      </div>
    </div>
    <!-- fin row 1 -->

    <hr>

    <!-- row 2 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información complementaria</h5>
      </div>

      <div class="col-md-8">

        <div class="form-group">
          <label>Contacto:</label> <br>
          <small>
            <label class="text-muted">
              Ingrese una forma de contacto escribiendo el nombre de la red social,
              dos puntos (:)
              y luego el nombre de usuario. Ejemplo: <strong>instagram:luciano27</strong>.
              Ingrese varias formas de contacto separados por espacio ( ). Ejemplo:
              <strong>instagram:veritatis facebook:voluptas email:nadia81@terra.com</strong>.
            </label>
          </small>
          <input class="form-control"
                 type="text"
                 required
                 name="contacto"
                 value="{{ old('contacto') }}">
          @if ($errors->has('contacto'))
            <div class="alert alert-danger">
              {{ $errors->first('contacto') }}
            </div>
          @endif
        </div>

        <div class="form-group">
          <label>Imagen:</label>
          <div class="custom-file"
               style="display:none">
            <input id="imagen"
                   class="custom-file-input"
                   type="file"
                   name="imagen"
                   lang="es">
            <label class="custom-file-label"
                   for="imagen">Elegir imagen</label>
          </div>
          <subir-imagen>@include('parciales.cargando-img')</subir-imagen>

          @if ($errors->has('imagen'))
            <div class="alert alert-danger">
              {{ $errors->first('imagen') }}
            </div>
          @endif
        </div>

      </div>
    </div>
    <!-- fin row 2 -->

    <hr>

    <div class="d-flex justify-content-center">
      <button type="submit" class="btn btn-primary mr-2">Guardar</button>
      <a class="btn btn-secondary" href="{{ route('admin.lideres.index') }}">Cancelar</a>
    </div>

  </form>


@endsection
@push('script')
<script>
  $('#imagen').on('change',function(){
    var fileName = $(this).val().replace('C:\\fakepath\\', '');
    $(this).next('.custom-file-label').html(fileName);
  });

  // valor de dropdown posicion
  $('#posicion').val(@json(old('posicion'))
    ? @json(old('posicion'))
    : null).change();


</script>
@endpush

