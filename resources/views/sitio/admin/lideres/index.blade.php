@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Listado de Líderes')
@push('style')
  @routes
@endpush
@section('admin_contenido')
    <h1>Listado de líderes</h1>

    <a class="btn btn-primary mb-1" href="{{ route('admin.lideres.create') }}">Crear Líder</a>

    <!-- cargar tabla vue -->
    <listado-lideres>@include('parciales.cargando-img')</listado-lideres>

@endsection
