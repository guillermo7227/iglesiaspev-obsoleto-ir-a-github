@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Crear Medio')
@section('admin_contenido')
  <h1>Crear Medio</h1>

  <hr>

  <form action="{{ route('admin.medios.store') }}"
        method="POST">
    @csrf

    <!-- row 1 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información general</h5>
      </div>

      <div class="col-md-8">

        <div class="form-group">
          <label>Tipo de medio:</label>
          <select id="tipo"
                  class="selectpicker form-control"
                  title="Seleccione una opción"
                  data-live-search="true"
                  required
                  name="tipo">
            @foreach(TIPOS_MEDIO as $tipo)
              <option value="{{ $tipo }}">{{ $tipo }}</option>
            @endforeach
          </select>
          @if ($errors->has('tipo'))
            <div class="alert alert-danger">
              {{ $errors->first('tipo') }}
            </div>
          @endif
        </div>


        <div class="form-group">
          <label>Descripción:</label>
          <input class="form-control"
                 type="text"
                 name="descripcion"
                 required
                 value="{{ old('descripcion') }}">
          @if ($errors->has('descripcion'))
            <div class="alert alert-danger">
              {{ $errors->first('descripcion') }}
            </div>
          @endif
        </div>


        <div class="form-group">
          <label>
            Url del medio:
            <span class="text-muted">(P.ej. https://youtube.com/watch?v=xH7jAiKu76y)</span>
          </label>
          <input class="form-control"
                 type="text"
                 name="url"
                 required
                 value="{{ old('url') }}">
          @if ($errors->has('url'))
            <div class="alert alert-danger">
              {{ $errors->first('url') }}
            </div>
          @endif
        </div>

      </div>
    </div>
    <!-- fin row 1 -->

    <hr>

    <div class="d-flex justify-content-center">
      <button type="submit" class="btn btn-primary mr-2">Guardar</button>
      <a class="btn btn-secondary" href="{{ route('admin.medios.index') }}">Cancelar</a>
    </div>

  </form>


@endsection
@push('script')
  <script>
    // valor de dropdown tipo medio
    $('#tipo').val(@json(old('tipo'))
      ? @json(old('tipo'))
      : null).change();

  </script>
@endpush
