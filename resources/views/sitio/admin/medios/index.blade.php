@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Listado de Medios')
@push('style')
  @routes
@endpush
@section('admin_contenido')
    <h1>Listado de medios</h1>

    <a class="btn btn-primary mb-1" href="{{ route('admin.medios.create') }}">Crear Medio</a>

    <!-- cargar tabla vue -->
    <listado-medios>@include('parciales.cargando-img')</listado-medios>

@endsection
