@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Editar Serie ' . $serie->titulo)
@section('admin_contenido')
  <h1>Editar serie</h1>

  <hr>

  <form action="{{ route('admin.series.update', $serie->slug) }}"
        method="POST">
    @method('PUT')
    @csrf

    <!-- row 1 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información general</h5>
      </div>

      <div class="col-md-8">

        <div class="form-group">
          <div class="form-group">
            <label>Titulo:</label>
            <input class="form-control"
                   type="text"
                   name="titulo"
                   value="{{ old('titulo') ?? $serie->titulo }}">
            @if ($errors->has('titulo'))
              <div class="alert alert-danger">
                {{ $errors->first('titulo') }}
              </div>
            @endif
          </div>
        </div>


        <div class="form-group">
          <div class="form-group">
            <label>Descripción:</label>
            <textarea class="form-control"
                      name="descripcion"
                      rows="5"
                      value="">{{ old('descripcion') ?? $serie->descripcion }}</textarea>
            @if ($errors->has('descripcion'))
              <div class="alert alert-danger">
                {{ $errors->first('descripcion') }}
              </div>
            @endif
          </div>
        </div>

      </div>
    </div>
    <!-- fin row 1 -->

    <hr>

    <div class="d-flex justify-content-center">
      <button type="submit" class="btn btn-primary mr-2">Guardar</button>
      <a class="btn btn-secondary" href="{{ route('admin.series.index') }}">Cancelar</a>
    </div>

  </form>


@endsection

