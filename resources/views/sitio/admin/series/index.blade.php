@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Listado de Series')
@push('style')
  @routes
@endpush
@section('admin_contenido')
    <h1>Listado de series</h1>

    <a class="btn btn-primary mb-1" href="{{ route('admin.series.create') }}">Crear Serie</a>

    <!-- cargar tabla vue -->
    <listado-series>@include('parciales.cargando-img')</listado-series>

@endsection
