@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Crear Sermón')
@push('style')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
@endpush
@section('admin_contenido')
  <h1>Crear sermón</h1>

  <hr>

  <form action="{{ route('admin.sermones.store') }}"
        method="POST">
    @csrf

    <!-- row 1 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información general</h5>
      </div>

      <div class="col-md-8">
        <div class="row">
          <div class="col-md-8">

            <div class="form-group">
              <div class="form-group">
                <label>Titulo:</label>
                <input class="form-control"
                       type="text"
                       name="titulo"
                       required
                       value="{{ old('titulo') }}">
                @if ($errors->has('titulo'))
                  <div class="alert alert-danger">
                    {{ $errors->first('titulo') }}
                  </div>
                @endif
              </div>
            </div>

          </div>

          <div class="col-md-4">

            <div class="form-group">
              <div class="form-group">
                <label>Fecha:</label>
                <input class="form-control"
                       type="date"
                       required
                       name="fecha"
                       value="{{ old('fecha') }}">
                @if ($errors->has('fecha'))
                  <div class="alert alert-danger">
                    {{ $errors->first('fecha') }}
                  </div>
                @endif
              </div>
            </div>

          </div>

        </div>

        <div class="form-group">
          <div class="form-group">
            <label>Descripción:</label>
            <textarea class="form-control"
                      name="descripcion"
                      rows="5"
                      required
                      value="">{{ old('descripcion') }}</textarea>
            @if ($errors->has('descripcion'))
              <div class="alert alert-danger">
                {{ $errors->first('descripcion') }}
              </div>
            @endif
          </div>
        </div>

      </div>
    </div>
    <!-- fin row 1 -->

    <hr>

    <!-- row 2 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información del pasaje bíblico</h5>
      </div>

      <div class="col-md-8">

        <div class="row">

          <div class="col-md-6">
            <div class="form-group">
              <label>Libro:</label>
              <select id="libro"
                      class="selectpicker form-control"
                      data-live-search="true"
                      title="Seleccione una opción"
                      required
                      name="libro">
                @foreach(LIBROS as $libro)
                  <option value="{{ $libro }}">{{ $libro }}</option>
                @endforeach
              </select>
              @if ($errors->has('libro'))
                <div class="alert alert-danger">
                  {{ $errors->first('libro') }}
                </div>
              @endif
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <div class="form-group">
                <label>Pasaje:</label>
                <input class="form-control"
                       type="text"
                       required
                       name="versiculos"
                       value="{{ old('versiculos') }}">
                   @if ($errors->has('versiculos'))
                     <div class="alert alert-danger">
                       {{ $errors->first('versiculos') }}
                     </div>
                   @endif
              </div>
            </div>
          </div>

        </div>

        @component('componentes.entidad-select', [
          'etiqueta' => 'Serie',
          'campo' => 'serie_id',
          'campoAMostrar' => 'titulo',
          'entidades' => $series,
          'ruta' => 'series',
        ])
        @endcomponent

      </div>

    </div>
    <!-- fin row 2 -->

    <hr>

    <!-- row 3 -->
    <div class="row">
      <div class="col-md-4">
        <h5>Información adicional</h5>
      </div>

      <div class="col-md-8">

        @component('componentes.entidad-select', [
          'etiqueta' => 'Predicador',
          'campo' => 'lider_id',
          'campoAMostrar' => 'nombre',
          'entidades' => $lideres,
          'ruta' => 'lideres',
        ])
        @endcomponent


        @component('componentes.entidad-select', [
          'etiqueta' => 'Multimedio',
          'campo' => 'medio_id',
          'campoAMostrar' => 'descripcion',
          'entidades' => $medios,
          'ruta' => 'medios',
          'campoAMostrarSegundario' => 'tipo'
        ])
        @endcomponent

      </div>
    </div>
    <!-- fin row 3 -->

    <hr>

    <div class="d-flex justify-content-center">
      <button type="submit" class="btn btn-primary mr-2">Guardar</button>
      <a class="btn btn-secondary" href="{{ route('admin.sermones.index') }}">Cancelar</a>
    </div>

  </form>


@endsection

@push('script')
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <script>
    // valor de dropdown libro
    $('#libro').val(@json(old('libro'))
      ? @json(old('libro'))
      : null).change();

    // valor de dropdown serie_id
    $('#serie_id').val(@json(old('serie_id'))
      ? @json(old('serie_id'))
      : null).change();

    // valor de dropdown lider_id
    $('#lider_id').val(@json(old('lider_id'))
      ? @json(old('lider_id'))
      : null).change();

    // valor de dropdown medio_id
    $('#medio_id').val(@json(old('medio_id'))
      ? @json(old('medio_id'))
      : null).change();

  </script>
@endpush
