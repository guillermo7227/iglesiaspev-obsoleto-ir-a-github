@extends('sitio.admin.layout.admin_master')
@section('titulo', 'Listado de Sermones')
@push('style')
  @routes
@endpush
@section('admin_contenido')
  <h1>Listado de sermones</h1>

  <a class="btn btn-primary mb-1" href="{{ route('admin.sermones.create') }}">Crear Sermón</a>

  <!-- cargar tabla vue -->
  <listado-sermones>@include('parciales.cargando-img')</listado-sermones>

@endsection
