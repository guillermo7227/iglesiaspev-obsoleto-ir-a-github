<div id="cuenta-atras" class="container pt-5">
  <div class="row">
    <div class="col text-center">
      <h3>Nuestro próximo servicio de adoración comienza en</h3>

      <div id="myCounter"></div>
    </div>
  </div>
</div>

@push('style')
  <link rel="stylesheet" href="{{ asset('css/mb-comingsoon.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('js/jquery.mb-comingsoon.min.js') }}"></script>
<script>

  // day: 0=Sunday, 1=Monday...4=Thursday...
  function nextDayAndTime(dayOfWeek, hour, minute) {
    var now = new Date()
    var result = new Date(
                   now.getFullYear(),
                   now.getMonth(),
                   now.getDate() + (7 + dayOfWeek - now.getDay()) % 7,
                   hour,
                   minute)

    if (result < now)
      result.setDate(result.getDate() + 7)

    return result
  }

  function comenzarContador(counter, fecha) {
    $('#myCounter').mbComingsoon({
      expiryDate: fecha,
      interval: 1, 		//Counter uopdate interval
      localization: {
        days: "días", 		//Localize labels of counter
        hours: "horas",
        minutes: "minutos",
        seconds: "segundos"
      },
      speed: 1,			//Animation duration in milliseconds from 0 to interval
    });
  }

  comenzarContador(null, nextDayAndTime(0, 9, 30));
</script>
@endpush

