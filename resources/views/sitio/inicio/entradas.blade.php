<div class="container py-5">

  <div class="row mb-3">
    <div class="col text-center">
      <h2 class="text-primary font-weight-bold">ENTRADAS DEL BLOG</h2>
    </div>
  </div>

  @if (count($entradas) > 0)
    <div class="row">
      @foreach($entradas as $entrada)
        @continue($loop->index == 2 || $loop->index == 3)
        <div class="col-md-6 my-2">
          @component('componentes.entrada', ['entrada' => $entrada])
          @endcomponent
        </div>
      @endforeach
    </div>

    <div class="row">
      @foreach($entradas as $entrada)
        @continue($loop->index == 0 || $loop->index == 1)
        <div class="col-md-6 my-2">
          @component('componentes.entrada', ['entrada' => $entrada])
          @endcomponent
        </div>
      @endforeach
    </div>
  @else
    <div class="text-center">
      No hay entradas para mostrar
    </div>
  @endif


  <div class="row">
    <div class="col text-right">
      <a href="{{ URL_BLOG }}" title="Ver mas entradas del Blog">Ver mas entradas...</a>
    </div>
  </div>
</div>

