<div class="container py-5">
  <div class="row">
    <div class="col-md-8">
      <h2 class="text-primary">¿Qué es el Evangelio?</h2>

      <p>
      Al evangelio se le llama “buenas nuevas” ya que habla sobre el problema más serio que tú y yo tenemos como seres humanos, y ese problema es simplemente esto: Dios es santo y Él es justo, y yo no lo soy. Y al final de mi vida estaré delante de un Dios justo y santo, y seré juzgado. Y seré juzgado ya sea en base a mi propia justicia, la falta de ella, o en base a la justicia de otro. Las buenas nuevas del evangelio son que Jesús vivió una vida de perfecta rectitud y perfecta obediencia a Dios, no a su propio favor, sino por su pueblo. Él ha hecho por mí lo que yo no podía hacer por mí mismo. Pero no solo vivió esa vida de perfecta obediencia, sino que se ofreció a sí mismo como un sacrificio perfecto para satisfacer la justicia de Dios.
      </p>

      <p>
      ¿Cómo nos apropiamos subjetivamente de los beneficios de Jesús? ¿Cómo los consigo? La Biblia deja en claro que no somos justificados por nuestras obras, ni por nuestros esfuerzos, ni por nuestras acciones, sino por la fe —y solo mediante la fe. La única manera en que puedes recibir el beneficio de la vida y la muerte de Cristo es poniendo tu fe en Él y solo en Él. Si haces esto, eres declarado justo por Dios, adoptado en su familia, perdonado de todos tus pecados, y habrás comenzado tu peregrinación hacia la eternidad.
      </p>
    </div>

    <div class="col-md-4 d-flex">
      <a href="#" onclick="verVideo(event)" class="my-auto mx-auto">
        <img src="{{ asset('img/evangelio_play.jpg') }}" alt="Video del evangelio" title="Ver video" height="200">
      </a>
    </div>
  </div>
</div>

@push('script')
<script>
  function verVideo(ev) {
    ev.preventDefault();

    const html = `
      <div class="video-responsive">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/GWtMu1F8-K4"
                frameborder="0" allow="accelerometer; autoplay=1; mute=1; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen>
        </iframe>
      </div>
    `;

    bootbox.dialog({
      closeButton: false,
      message: html,
      size: 'large',
      centerVertical: true,
      onEscape: true,
      backdrop: true,
    });
  }
</script>
@endpush
