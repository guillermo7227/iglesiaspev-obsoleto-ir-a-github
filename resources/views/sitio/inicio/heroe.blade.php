@push('style')
  <style>
    header .navbar {
      background-color: transparent !important;
    }
  </style>
@endpush
<div class="container-fluid hero mx-0 px-0">
  <div class="banner vh-100 text-center d-flex align-items-center justify-content-center">

    <div class="container">
      <h1 class="mx-md-5 px-md-5 pt-5">Enseñando sana doctrina para la gloria de Dios.</h1>

      <h5 class="sans my-4">Servicio de adoración los domingos a las 9:30 am.</h5>

      <a href="{{ route('sitio.contacto') }}" class="btn btn-primary mr-2">VISÍTENOS</a>
      <a href="{{ route('sitio.lo_que_ensenamos') }}" class="btn btn-secondary">LO QUE ENSEÑAMOS</a>
    </div>
  </div>

</div>
@push('script-urgente')
  <script>
    document.querySelector('header .navbar').classList.add('bg-transparent');
  </script>
@endpush
