<div class="container-fluid sermones bg-dark text-light py-5 px-5"
     style="background:url('{{ asset('img/predica.jpg') }}')">

  <div class="row">
    <div class="col text-center">
      <h2 class="text-primary font-weight-bold">SERMONES RECIENTES</h2>
    </div>
  </div>

  <div class="row mt-3">
    @if (count($sermones) == 0)
      <div class="col text-center">No hay sermones que mostrar</div>
    @else
      <div class="col-md-7">
        @component('componentes.sermon_reciente', ['sermon' => $sermones[0]])
        @endcomponent
      </div>

      <div class="col-md-5 d-flex flex-column justify-content-end">
        @foreach($sermones as $sermon)
          @continue($loop->first)
          <div class="pt-2">
            @include('parciales.sermon-span', ['claseParcial' => 'text-reset h5'])
            <small>
              <p class="text-muted">
                @include('parciales.pasaje-span', [
                  'libro' => $sermon->libro,
                  'claseParcial' => 'text-reset',
                ])-
                @include('parciales.lider-span', [
                  'lider' => $sermon->lider,
                  'claseParcial' => 'text-reset',
                ])-
                {{ $sermon->fechaFormateada }}
              </p>
            </small>
          </div>
        @endforeach
      </div>
    @endif
  </div>

  <div class="row">
    <div class="col text-right">
      <a href="{{ route('sermones.sermones.index') }}" title="Ver mas sermones">Ver mas sermones...</a>
    </div>
  </div>
</div>

