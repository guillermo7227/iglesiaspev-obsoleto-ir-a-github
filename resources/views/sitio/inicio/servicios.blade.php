<div class="container-fluid bg-primary py-5 px-5" style="color:#374140">
  <div class="row">
    <div class="col-md-5 my-2 d-flex flex-column justify-content-between">
      <h2 class="text-oscuro text-center text-lg-left">Nuestros Servicios</h2>

      <div class="row mb-3">
        <div class="col col-lg-auto text-center text-lg-left ">
          <p class="mb-0">
            <i class="text-oscuro fa fa-praying-hands text-center" style="width:30px"></i>
            Reunión de oración.
          </p>
        </div>

        <div class="col-lg text-center text-lg-left px-0">
          <small class="text-claro">Miércoles a las 6:30 pm</small>
        </div>
      </div>

      <div class="row mb-3">
        <div class="col col-lg-auto text-center text-lg-left ">
          <p class="mb-0">
            <i class="text-oscuro fa fa-chalkboard-teacher text-center" style="width:30px"></i>
            Escuela dominical.
          </p>
        </div>

        <div class="col-lg text-center text-lg-left px-0">
          <small class="text-claro">Domingos a las 8:30 am</small>
        </div>
      </div>

      <div class="row mb-3">
        <div class="col col-lg-auto text-center text-lg-left ">
          <p class="mb-0">
            <i class="text-oscuro fa fa-place-of-worship text-center" style="width:30px"></i>
            Culto de adoración.
          </p>
        </div>

        <div class="col-lg text-center text-lg-left px-0">
          <small class="text-claro">Domingos a las 9:30 am</small>
        </div>
      </div>
    </div>

    <div class="col-md-7 my-2 d-flex flex-column align-items-start">
      <h4 class="mt-auto mb-0 text-oscuro">Suscríbete a nuestro boletín mensual</h4>
      <p>Recibe un resumen mensual de lo que ocurre en nuestra iglesia (noticias, artículos y sermones).</p>

      <form action="{{ route('sitio.suscribirse_boletin') }}" method="POST" class="w-100">
        @csrf

        <div class="row">
          <div class="form-group col-md-4">
            <label>Nombre:</label>
            <input class="form-control" type="text" name="nombre" required>
          </div>

          <div class="form-group col-md-4">
            <label>Email:</label>
            <input class="form-control" type="email" name="email" required>
          </div>

          <div class="form-group col-md-4 d-flex">
            <input class="form-control mt-auto btn btn-secondary" type="submit" value="Suscribirse">
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
