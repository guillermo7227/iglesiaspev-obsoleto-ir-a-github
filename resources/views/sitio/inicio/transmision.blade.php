@if($transmision->transmitiendo)
  <div class="container py-5 px-5">

    <p class="text-center text-danger">Estamos transmitiendo en vivo en este momento.</p>
    <p class="text-center">
      <span class="text-muted">Título:</span>
      {{ $transmision->titulo }}
    </p>
    <div class="text-center video-responsive">
      {!! $transmision->codigo !!}
    </div>

  </div>

  <script>
    const cuentaAtras = document.querySelector('#cuenta-atras');
    cuentaAtras.style.display = 'none';
  </script>

@endif
