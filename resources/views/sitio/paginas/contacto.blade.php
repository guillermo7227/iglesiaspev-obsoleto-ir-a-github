@extends('layout.master')
@section('titulo', 'Contacto')
@section('contenido')
  <div class="container my-5">
    <h1 class="text-center text-primary font-weight-bold">CONTÁCTENOS</h1>

    <p>
    Puede contactarnos visitándonos o enviándonos un mensaje. Igualmente puede escribirnos en nuestras redes sociales.
    </p>

    <hr class="my-5">

    <div class="row">
      <div class="col-md-5">
        <h2>Visítenos</h2>

        <div class="h5">
          <span class="">Iglesia Cristiana Su Palabra es Verdad</span><br>
          <span class="text-muted">Calle 25 # 17-76 Barrio Simón Bolívar</span><br>
          <span class="text-muted">Valledupar, Colombia</span>
        </div>

        <br>

        <h2>Nuestros Servicios</h2>

        <div class="h5 mb-2 d-flex align-items-center">
          <div class="mr-3 text-muted">
            <i class="fa fa-xl fa-praying-hands"></i>
          </div>
          <div>
            <span class="">Reunión de oración</span><br>
            <span class="text-muted">Miércoles, 6:30pm.</span><br>
          </div>
        </div>

        <div class="h5 mb-2 d-flex align-items-center">
          <div class="mr-3 text-muted">
            <i class="fa fa-xl fa-chalkboard-teacher"></i>
          </div>
          <div>
            <span class="">Escuela dominical</span><br>
            <span class="text-muted">Domingos, 8:30am.</span><br>
          </div>
        </div>

        <div class="h5 mb-2 d-flex align-items-center">
          <div class="mr-3 text-muted">
            <i class="fa fa-xl fa-place-of-worship"></i>
          </div>
          <div>
            <span class="">Culto de adoración</span><br>
            <span class="text-muted">Domingos, 9:30am.</span><br>
          </div>
        </div>

      </div>

      <div class="col-md-7">
        <div class="mapa d-flex justify-content-end">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1961.7583761658598!2d-73.24728856340025!3d10.459868504498754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e8ab995fc39e9c3%3A0xb632f262cb41757f!2sCl.%2025%20%2317-76%2C%20Valledupar%2C%20Cesar!5e0!3m2!1ses-419!2sco!4v1570806022530!5m2!1ses-419!2sco" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
      </div>

    </div>

    <hr class="my-5">

    <div class="row">

{{--
      <div class="col-md-6">
        <h2>Envíenos un mensaje</h2>
        <form action="{{ route('sitio.contacto') }}" method="POST">
          @csrf

          <div class="row">
            <div class="form-group col-md-6">
              <label for="">Nombre:</label>
              <input type="text" name="nombre" required class="form-control">
            </div>

            <div class="form-group col-md-6">
              <label for="">E-mail:</label>
              <input type="email" name="email" required class="form-control">
            </div>

          </div>

          <div class="form-group">
            <label for="">Mensaje:</label>
            <textarea class="form-control" name="mensaje" required rows="4"></textarea>
          </div>

          <input class="btn btn-primary" type="submit" value="Enviar">
        </form>
      </div>
--}}
      <div class="col-md-6">
        <h2>Nuestras redes sociales</h2>

        <ul>
          <li><a href="https://api.whatsapp.com/send?phone=573024229912&text=Hola,%20quisiera%20contactar%20con%20ustedes..." target="_blank">Escríbanos a nuestro Whatsapp</a></li>
          <li><a href="https://www.facebook.com/Iglesia-Cristiana-Su-Palabra-es-Verdad-1532157290211246" target="_blank">Página de Facebook Iglesia Su Palabra es Verdad</a></li>
          <li><a href="https://www.youtube.com/channel/UCNaQFamYazyKVzVl-_pEB-Q" target="_blank">Canal Youtube Iglesia Su Palabra es Verdad</a></li>
        </ul>
      </div>
    </div>


  </div>
@endsection

