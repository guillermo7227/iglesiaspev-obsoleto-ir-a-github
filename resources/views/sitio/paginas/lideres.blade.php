@extends('layout.master')
@section('titulo', 'Nuestros líderes')
@section('contenido')
  <div class="container my-5">
    <h1 class="text-primary text-center font-weight-bold">NUESTROS LÍDERES</h1>

    <p>
    Somos una iglesia que cree en la pluralidad de líderes, ancianos y diáconos que gobiernen y administren los asuntos de la iglesia.
    Por la gracia de Dios, contamos con la ayuda y colaboración de los pastores de nuestra iglesia madre, la iglesia Vida en Su Palabra de Riohacha - La Guajira,
    que frecuentemente nos visitan para edificarnos con la palabra de verdad.
    </p>

    <hr class="my-5">

    <div class="row">
      @foreach ($lideres as $lider)
        @continue($lider->retirado == true)
        @continue($lider->posicion == 'Líder')
        <div id="lider-{{ $lider->id }}" class="col-md-6 pb-5 px-4">
          <h2 class="text-primary">
            @include('parciales.lider-span')
            <small class="text-muted">{{ $lider->posicion }}</small>
          </h2>
          <div class="row">
            <div class="col-md-4">
              <img class="img-fluid my-2 foto-lider" src="{{ asset($lider->imagen) }}">

              <p class="text-muted text-center w-100">
                {{--
                @if (isset($lider->contactoArr['facebook']))
                  <a href="https://fb.me/{{ $lider->contactoArr['facebook'] }}" target="_blank">
                    <i class="fab fa-facebook mr-2"></i>
                  </a>
                @endif

                @if (isset($lider->contactoArr['instagram']))
                  <a href="https://instagram.com/{{ $lider->contactoArr['instagram'] }}" target="_blank">
                    <i class="fab fa-instagram mr-2"></i>
                  </a>
                @endif

                @if (isset($lider->contactoArr['email']))
                  <a href="mailto:{{ $lider->contactoArr['email'] }}">
                    <i class="fa fa-envelope"></i>
                  </a>
                @endif
                --}}
              </p>
            </div>

            <div class="col-md-8">
              <p>{{ $lider->bio }}</p>
            </div>
          </div>
        </div>

      @endforeach
    </div>

    <div class="my-2">
      <h2>Colaboradores de predicación y enseñanza</h2>
      <br>
      @foreach ($lideres as $lider)
        @continue($lider->retirado == true)
        @continue($lider->posicion != 'Líder')
        <div id="lider-{{ $lider->id }}" class="pb-2 px-2">
          {{-- <img class="mb-2 mr-2 foto-lider" src="{{ asset($lider->imagen) }}" width="70"> --}}
          <h5 class="text-primary mr-2">
            @include('parciales.lider-span')
            <small class="text-muted mr-2">{{ $lider->posicion }}</small>

            <span class="text-muted text-center">
              {{--
              @if (isset($lider->contactoArr['facebook']))
                <a href="https://fb.me/{{ $lider->contactoArr['facebook'] }}" target="_blank">
                  <i class="fab fa-sm fa-facebook mr-2"></i>
                </a>
              @endif

              @if (isset($lider->contactoArr['instagram']))
                <a href="https://instagram.com/{{ $lider->contactoArr['instagram'] }}" target="_blank">
                  <i class="fab fa-sm fa-instagram mr-2"></i>
                </a>
              @endif

              @if (isset($lider->contactoArr['email']))
                <a href="mailto:{{ $lider->contactoArr['email'] }}">
                  <i class="fa fa-sm fa-envelope"></i>
                </a>
              @endif
              --}}
            </span>

          </h5>
          <div class="">
            <p>{{ $lider->bio }}</p>
          </div>
        </div>

      @endforeach
    </div>

  </div>
@endsection
