@extends('layout.master')
@section('titulo', 'Lo que enseñamos')
@section('contenido')
  <div class="container my-5">
    <h1 class="text-primary text-center font-weight-bold">LO QUE ENSEÑAMOS</h1>

    <div class="row py-5">

      <div class="col-md-4 text-center d-flex flex-column align-items-center">
        <div class="rounded-circle img-fondo img-circulo"
             style="background:url('{{ asset('img/cincosolas.jpg') }}')">
        </div>
        <div>
          <p class="font-weight-bold mb-0">
          <a href="#cinco_solas">Las cinco Solas de la reforma</a>
          </p>
          <small>
            <p class="">
            Somos una iglesia fiel, por la gracia de Dios, a las ideas de los reformadores originales y a sus deseos 
            de mantener pura las enseñanzas de los apóstoles.
            </p>
          </small>
        </div>
      </div>

      <div class="col-md-4 text-center d-flex flex-column align-items-center">
        <div class="rounded-circle img-fondo img-circulo"
             style="background:url('{{ asset('img/tulipan.jpg') }}')">
        </div>
        <div>
          <p class="font-weight-bold mb-0">
          <a href="#doctrinas_gracia">Las doctrinas de la gracia</a>
          </p>
          <small>
            <p class="">
            Predicamos lo que consideramos que enseña la Biblia sobre la gracia, los 5 puntos del Calvinismo, también conocidos como TULIP.
            </p>
          </small>
        </div>
      </div>

      <div class="col-md-4 text-center d-flex flex-column align-items-center">
        <div class="rounded-circle img-fondo img-circulo"
             style="background:url('{{ asset('img/confesion1689.jpg') }}')">
        </div>
        <div>
          <p class="font-weight-bold mb-0">
          <a href="#confesion_1689">Confesión de Fe de 1689</a>
          </p>
          <small>
            <p class="">
            Nos adherimos a lo que consignaron los santos de mitad del siglo XX, en la confesión bautista de fe de Londres de 1869.
            </p>
          </small>
        </div>
      </div>

    </div>

    <hr class="my-5">

    @include('sitio.paginas.lo_que_ensenamos.cinco_solas')

    <hr class="my-5">

    @include('sitio.paginas.lo_que_ensenamos.doctrinas_gracia')

    <hr class="my-5">

    @include('sitio.paginas.lo_que_ensenamos.confesion_1689')
  </div>
@endsection
