<h2 id="cinco_solas" class="text-primary font-weight-bold">Las cinco solas de la reforma protestante</h2>

<div class="w-100 py-5 img-fondo" style="background:url('{{ asset('img/cincosolas.jpg') }}')"></div>

<p>
Los protestantes no son católicos romanos. Los católicos romanos tampoco son protestantes.
</p>

<p>
Aunque estén de acuerdo en varios asuntos doctrinales, hay una pequeñita palabra que los separa una y otra vez: la palabra “solo” o “sola”.
</p>

<p>
Al igual que los protestantes, los católicos romanos creen en la autoridad de las Escrituras, la importancia de la gracia, la excelencia de Cristo, la necesidad de fe, y la adoración a Dios. No obstante, a diferencia del protestantismo, el catolicismo nunca habla de la sola Escritura, la sola gracia, solo Cristo, la sola fe, o de dar solo a Dios la gloria.
</p>

<p>
Quisiera explicar brevemente de lo que se tratan las denominadas cinco “solas” del protestantismo. En realidad, estos cinco puntos teológicos son los que mantienen Wittenberg y el Vaticano irreconciliablemente separados.
</p>

<h3 class="text-primary">1. Sola Scriptura</h3>

<p>
Los protestantes confiesan su fe en la “sola Scriptura”. Esta doctrina, la cual fue el principio formal de la Reforma, enseña que la Biblia es la suprema norma de fe y conducta en la vida del pueblo de Dios. Conviene destacar que la “sola Scriptura” no equivale a la “nuda Scriptura”, es decir, la noción de que los creyentes solamente deberían leer la Biblia. Además de los grandes credos de la historia de la Iglesia —el apostólico, el niceno, el de Calcedonia— hay una gran selección de confesiones de fe y catecismos magistrales, además de un gran número de libros teológicos escritos por preciosos siervos de Dios, que nos animan en nuestra vida cristiana.
</p>

<p>
La “sola Scriptura”, pues, no niega el lugar de otras autoridades menores, pero sí garantiza que tales autoridades nunca pueden estar a la misma altura que la Palabra de Dios. En este sentido hay una clara discrepancia con el catolicismo ya que el sistema teológico romano coloca la autoridad de la tradición eclesiástica al lado de aquella de la Biblia como una fuente igualmente autoritativa.
</p>

<h3 class="text-primary">2. Sola gratia</h3>

<p>
La segunda “sola” tiene que ver con la “sola gratia” (sola gracia), es decir, que la salvación es cien por cien del Señor Dios Todopoderoso. Nuestros padres protestantes (Lutero, Zuinglio, Calvino) emplearon esta doctrina para negar la enseñanza erasmiana/humanista de que la salvación es un asunto sinergístico en el cual el ser humano puede cooperar con la gracia de Dios con el fin de llevar a cabo su propia salvación.
</p>

<p>
En el protestantismo, el único Salvador es el mismísimo Dios. Por lo tanto, el hombre no puede jactarse de sus méritos ni de sus buenas decisiones delante del Señor. Como lo expresó el himnólogo reformado Augustus Toplady (1740-78): “Solo Tú puedes salvar”. O en palabras de John Newton (1725-1807), “Sublime gracia del Señor, que a mí, pecador, salvó”.
</p>

<h3 class="text-primary">3. Solus Christus</h3>

<p>
La tercera “sola” se centra en la verdadera raíz de la salvación, el Señor Jesucristo. La gracia de Dios alcanza a los pecadores a través de la perfecta justicia del bendito Hijo de Dios. Los pecados del pueblo de Dios son puestos en la cuenta de Cristo y la justicia de Cristo está depositada en la cuenta del impío. La dogmática protestante llamaría esta gozosa noticia “la doctrina de la doble imputación” (2 Corintios 5:21).
</p>

<p>
Así que la obra de Cristo no se limita a quitar los pecados de su pueblo, sino de concederles una justicia positiva para que hereden el reino de los cielos. ¿Cómo realizó Cristo semejante logro? Mediante su obra impecable de expiación llevada a cabo desde su encarnación, culminando en su muerte, sepultura, resurrección, y ascensión. Es la intercesión continua de Cristo la que garantiza la salvación de los suyos. Por esta razón los protestantes han criticado la doctrina romana del purgatorio, puesto que tal enseñanza pone en tela de juicio la obra sacerdotal infalible del amado Jesús.
</p>

<h3 class="text-primary">4. Sola fide</h3>

<p>
La cuarta sola llegó a ser el principio material de la Reforma. Fue el mensaje de la justificación por la sola fe la que revolucionó la vida del monje agustiniano Martín Lutero (1483-1546). La salvación ya no se trataba de una cuestión de mérito humano, obras piadosas, justicia religiosa, o penitencia, como en el catolicismo, sino de un precioso regalo de Dios concedido al impío a través de un bendito canal llamado “fe”. Comentó Lutero en la Disputa de Heidelberg (1518): “La Ley dice: “Haz esto”, y eso jamás se hace; dice la gracia: “Cree en éste”, y todo está ya realizado”.
</p>

<p>
Ahora bien, la fe en sí nunca fue considerada como la base de la salvación en el protestantismo. Se trató de un simple medio mediante el cual Dios aplicó el mérito de Cristo al pecador. Al creer la buena noticia del Evangelio de la muerte y resurrección de Cristo, el creyente se encuentra totalmente justificado ante Dios porque la perfecta justicia del Hijo ahora es suya. Dios ya no ve al creyente en Adán el rebelde sino en Cristo el obediente.
</p>

<h3 class="text-primary">5. Soli Deo gloria</h3>

<p>
Dado que la salvación es totalmente del Señor —por su gracia a través de la fe en Cristo— la conclusión lógica de la confesión protestante es: ¡Soli Deo gloria! Traducido al castellano significa: “A Dios única y exclusivamente sea la gloria”.
</p>

<p>
Este lema ha llevado a multitudes de siervos del Señor a vivir para la gloria de Dios y la proclamación de su Palabra hasta los confines de la tierra a pesar de todo tipo de persecución brutal. Los protestantes no se doblan ante ningún enemigo —sea potente o no— porque solamente se postran ante el Rey de Reyes: Emanuel, el único Soberano.
</p>

<p>
Qué estas benditas “solas” corran por todo el mundo hispano en nuestra generación.
</p>

<p>
¡Sola Scriptura!

¡Sola gratia!

¡Solus Christus!

¡Sola fide!

¡Soli Deo gloria!
</p>

<small>
  <p>
    Fuente:
    <a href="https://www.coalicionporelevangelio.org/articulo/que-son-las-cinco-solas/" target="_blank">
      ¿Qué nos separa de los católicos? Las cinco solas de la reforma protestante. Coalición por el Evangelio.
    </a>
  </p>
</small>
