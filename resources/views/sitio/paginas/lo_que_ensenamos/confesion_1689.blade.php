<h2 id="confesion_1689" class="text-primary font-weight-bold">La Confesión Bautista de Fe de Londres (1689)</h2>

<div class="w-100 py-5 img-fondo" style="background:url('{{ asset('img/confesion1689.jpg') }}')"></div>

<p>
La Confesión de Fe Bautista de 1689 es una confesión cristiana (ortodoxa), es decir, que se adhiere a aquellas doctrinas que el cristianismo bíblico ha defendido a través de los siglos.
</p>

<p>
<strong class="text-primary">1. La Escritura.</strong> Nosotros creemos que la palabra de Dios es inspirada por Dios en todas sus partes y en cada una de sus palabras. Eso quiere decir que todo cuanto dice en este libro, en sus escritos originales, aunque fue escrito por hombres, fue escrito por hombres inspirados por el Espíritu Santo; y la Confesión de Londres se adhiere a esa doctrina.
</p>

<p>
¿A qué se opone? A la doctrina de los liberales y de los neo-ortodoxos. Los liberales dicen que en la Biblia hay muchas cosas folklóricas, que nosotros no podemos creerlas tal y cual están allí escritas.
</p>

<p>
Los neo-ortodoxos, por su parte, dicen que la Biblia no es la palabra de Dios, sino que contiene la palabra de Dios. Cuando usted está leyendo la Palabra, y hay algún texto que lo toca de manera particular, los neo-ortodoxos dicen: “En ese momento ese texto se convirtió en palabra de Dios para ti”. Muy sutil, pero equivocado.
La Biblia no contiene, sino que es, en todas sus partes, desde Génesis hasta Apocalipsis, la Palabra infalible e inerrante de Dios; y eso es lo que esta Confesión afirma, conforme a la enseñanza de la Escritura misma (2Tim. 3:16-17; 2P. 1:16-21).
</p>

<p>
<strong class="text-primary">2. Dios.</strong> Con respecto a Dios esta Confesión también se adhiere a la confesión de la Iglesia a través de los siglos. Creemos en un Dios que subsiste en tres personas: Dios el Padre, Dios el Hijo, y Dios el Espíritu Santo.
</p>

<p>
¿A qué error se opone? Al de los Testigos de Jehová, que nos dicen que Dios es uno solo, Jehová. Que la primera criatura de Dios fue el Hijo, y que el Espíritu Santo no es más que una fuerza, un poder.
</p>

<p>
La Biblia, en cambio, nos enseña que el Padre es Dios, el Hijo es Dios, el Espíritu Santo es Dios; pero aún así no hay tres Dioses, sino un solo Dios. El misterio de la Santísima Trinidad.
</p>

<p>
<strong class="text-primary">3. La Creación y la Providencia.</strong> En cuanto a la creación y la providencia la Confesión afirma que Dios creó el mundo y todo lo creado en seis días. Que el séptimo día descansó, y que de ahí en adelante Dios ha continuado gobernando su creación.
</p>

<p>
¿A qué error se opone? A los evolucionistas, por un lado, que nos dicen que este mundo es el producto de millones y millones y millones de años. Y también se opone a la doctrina de los deístas que dicen que Dios creó el mundo, pero después lo dejó funcionando solo. Pero la Biblia enseña que Dios creó el mundo y que Él gobierna su creación a través de la providencia.
</p>

<p>
<strong class="text-primary">4. Cristo.</strong> En cuanto a Cristo la Confesión de Fe afirma que Cristo es verdadero Dios y verdadero hombre; el Mesías que Dios había prometido en el Antiguo Testamento.
</p>

<p>
Se oponen en esto a los liberales, y a los Testigos de Jehová. Estos proclaman que Cristo fue un gran hombre, un gran maestro. Pero Cristo no era simplemente un gran maestro, ni un gran hombre únicamente, sino la segunda persona de la Trinidad encarnada.
</p>

<p>
<strong class="text-primary">5. El Evangelio.</strong> En cuanto al evangelio la Confesión afirma que el pecador es justificado por medio de la fe, sin las obras de la ley. Las grandes doctrinas que se defendieron en la época de la Reforma, y por la cual muchos de nuestros antepasados tuvieron que dar la vida en las mazmorras de la Inquisición Católico-romana.
</p>

<p>
¿A qué se opone? Precisamente al Catolicismo Romano que nos dice que el hombre no es justificado por la fe sola, sino también a través de sus buenas obras.
</p>

<p>
<strong class="text-primary">6. La Vida Cristiana.</strong> En cuanto a la vida cristiana la confesión de fe nos dice que el hombre es salvo por la fe sin obras, pero para buenas obras. Es cierto que somos salvos sin obras, pero una vez salvo el cristiano debe dar frutos de salvación (Ef. 2:8-10).
</p>

<p>
¿A qué se opone esta doctrina? A la credulidad de nuestros días. Ser crédulo no es tener una fe fuerte; ser crédulo es tener una fe ligera. Y hoy día muchas personas dicen que basta con creer que Jesucristo es el Hijo de Dios para ser salvos, no importa qué estilo de vida lleven de ahí en adelante.
</p>

<p>
<strong class="text-primary">7. El Mundo Venidero.</strong> En cuanto al mundo venidero la Confesión de Fe nos dice que habrá una resurrección de justos e injustos. La Confesión de Fe nos dice que hay dos lugares eternos: el cielo y el infierno.
</p>

<p>
¿A qué se opone? Por un lado se opone al universalismo, doctrina que enseña que al final todos serán salvos, incluyendo el diablo. Se opone también al aniquilacionismo, es decir la doctrina que enseña que una vez morimos todo termina allí; que no hay cielo ni hay infierno.
</p>

<p>
También hay aniquilacionistas que enseñan que hay cielo, pero que no hay infierno, como es el caso de los Testigos de Jehová. Los Adventistas del Séptimo Día a la larga son aniquilacionistas también, ellos dicen que todos los impíos serán destruidos, y no quedará más memoria de ellos.
</p>

<p>
Así que, como ustedes pueden ver, nuestra Confesión de Fe es cristiana ortodoxa, en el sentido de que defiende aquellas grandes doctrinas que el cristianismo bíblico ha defendido a través de los siglos.
</p>

<small>
  <p>
  Fuente:
  <a href="https://www.coalicionporelevangelio.org/entradas/sugel-michelen/1-la-confesion-de-fe-bautista-de-1689-es-una-confesion-cristiana-ortodoxa/" target="_blank">La Confesión de Fe Bautista de 1689. Coalición por el Evangelio.</a>
  </p>
</small>
