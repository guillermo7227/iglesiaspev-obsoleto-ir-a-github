<h2 id="doctrinas_gracia" class="text-primary font-weight-bold">Las doctrinas de la gracia</h2>

<div class="w-100 py-5 img-fondo" style="background:url('{{ asset('img/tulipan.jpg') }}')"></div>

<p>
La verdad central de la gracia salvadora de Dios se establece de forma resumida en la afirmación: “La salvación es del Señor”. Esta fuerte declaración significa que cada aspecto de la salvación del hombre proviene de Dios y depende totalmente de Dios. La única contribución que hacemos es el pecado que fue puesto sobre Jesucristo en la cruz. El apóstol Pablo afirmó esto cuando escribió: “Porque de Él, por Él y para Él son todas las cosas. A Él sea la gloria para siempre” (Ro. 11:36). Esto quiere decir que la salvación es determinada por Dios, comprada por Dios, aplicada por Dios y asegurada en Dios. De principio a fin, la salvación es solo del Señor.
</p>

<p>
La única contribución que hacemos es el pecado que fue puesto sobre Jesucristo en la cruz.
</p>

<p>
Esta verdad se resume mejor en las doctrinas de la gracia, que son: la depravación total, la elección incondicional, la expiación definida, el llamado eficaz, y la gracia que preserva. Estas verdades presentan al Dios trino como el autor de nuestra salvación de principio a fin. Cada miembro de la divinidad, Padre, Hijo, y Espíritu, juega un papel en la redención, y trabajan juntos como un solo Dios para rescatar a aquellos que perecen bajo la ira divina. En perfecta unidad, las tres personas divinas hacen el trabajo que los pecadores destinados al infierno, quienes son completamente incapaces de salvarse a sí mismos, no pueden hacer.
</p>

<h3 class="text-primary">Depravación total</h3>

<p>
El primer hombre, Adán, pecó, y su transgresión y culpa fueron inmediatamente imputadas a toda la humanidad (excepto Cristo). Con este único acto de desobediencia, contaminó moralmente cada parte de su ser: mente, afectos, cuerpo, y voluntad. Por este pecado, la muerte entró al mundo, y la comunión de Adán con Dios se rompió.
</p>

<p>
La culpa y la corrupción de Adán fue transmitida a su descendencia natural en el momento de la concepción. A su vez, cada uno de los hijos de sus hijos hereda esta misma caída radical. De manera consecuente se ha transmitido a cada generación hasta el día de hoy. La naturaleza perversa de Adán se ha extendido a la totalidad de cada persona.
</p>

<p>
Fuera de la gracia, nuestras mentes están oscurecidas por el pecado, incapaces de comprender la verdad. Nuestros corazones están contaminados, incapaces de amar la verdad. Nuestros cuerpos están muriendo, progresando hacia la muerte física. Nuestras voluntades están muertas, incapaces de elegir lo bueno. La incapacidad moral para agradar a Dios contamina a todas las personas desde su entrada en el mundo. En su estado no regenerado, nadie busca a Dios. Nadie es capaz de hacer el bien. Todos están bajo la maldición de la ley, que es la muerte eterna.
</p>

<h3 class="text-primary">Elección incondicional</h3>


<p>
Mucho antes de que Adán pecara, Dios ya había decretado y determinado la salvación para los pecadores. En la eternidad pasada, el Padre eligió a un pueblo que sería salvo en Cristo. Antes de que el tiempo comenzara, Dios eligió a muchos entre la humanidad a quienes se proponía salvar de su ira. Esta selección no se basó en ninguna fe prevista en aquellos a quienes eligió. Tampoco fue motivado por su bondad inherente. En su lugar, de acuerdo con su amor infinito y su sabiduría inescrutable, Dios puso su afecto en sus elegidos.
</p>

<p>
El Padre dio a los elegidos a su Hijo para ser su novia. Cada uno de los elegidos fue predestinado por el Padre para ser hecho a la imagen de su Hijo y cantar sus alabanzas para siempre. El Padre comisionó a su Hijo para venir este mundo y entregar su vida para salvar a estos mismos elegidos. Del mismo modo, el Padre comisionó al Espíritu para traer a estos mismos elegidos a la fe en Cristo. El Hijo y el Espíritu concurrieron libremente en todas estas decisiones, haciendo de la salvación la obra indivisible del Dios trino.
</p>

<p>
Cada uno de los elegidos fue predestinado por el Padre para ser hecho a la imagen de su Hijo y cantar sus alabanzas para siempre.
</p>

<h3 class="text-primary">Expiación definida</h3>

<p>
En la plenitud de los tiempos, Dios el Padre envió a su Hijo a entrar en este mundo caído con la misión de redimir a su pueblo. Nació de una virgen, sin naturaleza pecaminosa, para vivir una vida sin pecado. Jesús nació bajo la ley divina para obedecerla por completo en nombre de los pecadores desobedientes que la habían roto repetidamente. Esta obediencia activa de Cristo cumplió todas las justas exigencias de la ley. Al guardar la ley, el Hijo de Dios logró una justicia perfecta, la cual es contada a los pecadores creyentes para que sean declarados justos (justificados) ante Dios.
</p>

<p>
Esta vida sin pecado de Jesús lo calificó para ir a la cruz y morir en lugar de los pecadores culpables y destinados al infierno. En la cruz, Jesús soportó la completa ira del Padre por los pecados de su pueblo. En esta muerte vicaria, el Padre transfirió a su Hijo todos los pecados de todos aquellos que creerían en Él. Siendo un sacrificio y cargando el pecado, Jesús murió como sustituto en lugar de los elegidos de Dios. En la cruz, Él propició la justa ira de Dios hacia los elegidos. Por la sangre de la cruz, Jesús reconcilió al Dios santo con el hombre pecador, estableciendo la paz entre ambos. En su muerte redentora, Él compró a su novia, su pueblo elegido, de la esclavitud del pecado y la liberó.
</p>

<p>
La muerte de Jesús no solo hizo a toda la humanidad potencialmente salvable. Tampoco su muerte simplemente logró un beneficio hipotético que puede o no ser aceptado. Su muerte, tampoco, simplemente hizo a toda la humanidad redimible. En su lugar, Jesús en realidad redimió a un pueblo específico a través de su muerte, asegurando y garantizando su salvación. Ni una gota de la sangre de Jesús se derramó en vano. Él verdaderamente salvó a todos por quienes murió. Esta doctrina de la expiación definida se conoce en ocasiones como expiación limitada.
</p>

<h3 class="text-primary">Llamado eficaz</h3>

<p>
Con unidad de propósito, el Padre y el Hijo enviaron el Espíritu Santo al mundo para aplicar esta salvación a los elegidos y redimidos. El Espíritu vino a convencer a los elegidos de pecado, justicia, y juicio, y a volverse al Hijo, a todos aquellos que el Padre le dio. En el tiempo divinamente señalado, el Espíritu quita de cada elegido su incrédulo corazón de piedra, endurecido y muerto en pecado, y lo reemplaza con un corazón creyente de carne, receptivo y vivo para Dios. El Espíritu implanta vida eterna dentro del alma espiritualmente muerta. Él concede a los hombres y mujeres elegidos los dones del arrepentimiento y la fe, lo que les permite creer que Jesucristo es el Señor.
</p>

<p>
De repente, todas las cosas se vuelven nuevas. La nueva vida del Espíritu produce un nuevo amor por Dios. Nuevos deseos de obedecer la Palabra de Dios producen una nueva búsqueda de la santidad. Hay una nueva dirección de vida, vivida con una nueva pasión por Dios. Estos nacidos de nuevo dan evidencia de su elección con el fruto de la justicia. Este llamado del Espíritu es efectivo, lo que significa que los elegidos ciertamente responderán cuando se les dé dicho llamado. Finalmente no se resistirán. Por este motivo, la doctrina del llamado eficaz en ocasiones se le llama la doctrina de la gracia irresistible.
</p>

<h3 class="text-primary">Gracia que preserva</h3>
<p>
Una vez convertido, cada creyente se mantiene eternamente seguro por las tres personas de la Trinidad. A todos los que Dios conoció de antemano y predestinó en la eternidad pasada, glorificará en la eternidad futura. Ningún creyente abandonará a Dios o se apartará. Cada creyente está firmemente retenido por las manos soberanas del Padre, el Hijo, y el Espíritu Santo, y nunca se perderá. Ninguna de las ovejas por las cuales Jesús dio su vida perecerá. El Espíritu Santo sella de manera permanente en Cristo a todos los que atrae a la fe. Una vez nacido de nuevo, no podría no haber nacido. Una vez creyente, ninguno puede convertirse en incrédulo. Una vez salvo, ninguno puede dejar de serlo. Dios los preservará en la fe para siempre, y perseverarán hasta el fin. Es por esto que la doctrina de la gracia que preserva a menudo se llama la doctrina de la preservación de los santos.
</p>

<p>
Una vez salvo, ninguno puede dejar de serlo.
</p>

<p>
De principio a fin, la salvación es del Señor. En realidad, estas cinco doctrinas de la gracia forman un cuerpo completo de verdad con respecto a la salvación. Están inseparablemente conectadas y por lo tanto se mantienen o caen todas juntas. Abrazar cualquiera de las cinco requiere abrazar las cinco. Negar una es negar las otras y fracturar la Trinidad, poniendo a las tres personas en desacuerdo entre sí. Estas doctrinas hablan juntas a una sola voz para dar la mayor gloria a Dios. Esta alta teología produce alta doxología. Cuando se comprende correctamente que solo Dios: Padre, Hijo, y Espíritu, salva a los pecadores, entonces toda la gloria es para Él.
</p>

<small>
  Fuente:
  <a href="https://www.coalicionporelevangelio.org/articulo/las-doctrinas-la-gracia/"
     target="_blank">Las doctrinas de la gracia. Coalición por el Evangelio</a>
</small>
