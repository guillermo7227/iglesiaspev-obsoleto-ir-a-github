@extends('layout.master')
@section('titulo', 'Ministerios')
@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
@endpush
@section('contenido')
  <div class="container my-5">
    <h1 class="text-center text-primary font-weight-bold">MINISTERIOS</h1>
    <p>
      Duis viverra. Nulla diam lectus, tincidunt et, scelerisque vitae, aliquam
      vitae, justo. Quisque eget erat. Donec aliquet porta magna. Sed nisl. Ut
      tellus. Suspendisse quis mi eget dolor sagittis tristique. Aenean non pede eget
      nisl bibendum gravida. Class aptent taciti.
    </p>

    <hr class="my-5">

    <h2 class="text-primary">Cultivar un corazón generoso</h2>

      <div class="d-flex">
        <a href="{{ asset('img/cincosolas.jpg') }}"
           data-toggle="lightbox"
           data-gallery="galeria-cultivar">
            <img src="{{ asset('img/cincosolas.jpg') }}"
                 class="img-fluid img-galeria border rounded">
        </a>

        <a href="{{ asset('img/predica.jpg') }}"
           data-toggle="lightbox"
           data-gallery="galeria-cultivar" >
            <img src="{{ asset('img/predica.jpg') }}"
                 class="img-fluid img-galeria border rounded">
        </a>

        <a href="{{ asset('img/fondo_hero.jpg') }}"
           data-toggle="lightbox"
           data-gallery="galeria-cultivar" >
            <img src="{{ asset('img/fondo_hero.jpg') }}"
                 class="img-fluid img-galeria border rounded">
        </a>

        <a href="{{ asset('img/tulipan.jpg') }}"
           data-toggle="lightbox"
           data-gallery="galeria-cultivar" >
            <img src="{{ asset('img/tulipan.jpg') }}"
                 class="img-fluid img-galeria border rounded">
        </a>
      </div>

      <p>
      Diam quis tincidunt facilisis, sem quam luctus augue, ut posuere
      neque sem vitae neque.
      </p>
      <p>
      Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
      ridiculus mus. Nunc faucibus posuere turpis. Sed laoreet, est sed gravida
      tempor, nibh enim fringilla quam, et dapibus mi enim sit amet risus. Nulla
      sollicitudin eros sit amet diam. Aliquam ante. Vestibulum ante ipsum primis in
      faucibus orci luctus et ultrices posuere cubilia Curae; Ut et est. Donec semper
      nulla in ipsum. Integer elit. In pharetra lorem vel ante.
      </p>

      <p>
      Sed sed justo. Curabitur consectetuer arcu. Etiam placerat est eget odio. Nulla
      facilisi. Nulla facilisi. Mauris non neque. Suspendisse et.
      </p>




    <hr class="my-5">

    <h2 class="text-primary">Ministerio de mujeres</h2>

    <div class="d-flex">
      <a href="{{ asset('img/cincosolas.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar">
          <img src="{{ asset('img/cincosolas.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/predica.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/predica.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/fondo_hero.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/fondo_hero.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/tulipan.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/tulipan.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>
    </div>


    <p>
      Diam quis tincidunt facilisis, sem quam luctus augue, ut posuere
      neque sem vitae neque.
    </p>
    <p>
      Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
      ridiculus mus. Nunc faucibus posuere turpis. Sed laoreet, est sed gravida
      tempor, nibh enim fringilla quam, et dapibus mi enim sit amet risus. Nulla
      sollicitudin eros sit amet diam. Aliquam ante. Vestibulum ante ipsum primis in
      faucibus orci luctus et ultrices posuere cubilia Curae; Ut et est. Donec semper
      nulla in ipsum. Integer elit. In pharetra lorem vel ante.
    </p>

    <p>
      Sed sed justo. Curabitur consectetuer arcu. Etiam placerat est eget odio. Nulla
      facilisi. Nulla facilisi. Mauris non neque. Suspendisse et.
    </p>


    <hr class="my-5">

    <h2 class="text-primary">Ministerio de jóvenes</h2>

    <div class="d-flex">
      <a href="{{ asset('img/cincosolas.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar">
          <img src="{{ asset('img/cincosolas.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/predica.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/predica.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/fondo_hero.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/fondo_hero.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>

      <a href="{{ asset('img/tulipan.jpg') }}"
         data-toggle="lightbox"
         data-gallery="galeria-cultivar" >
          <img src="{{ asset('img/tulipan.jpg') }}"
               class="img-fluid img-galeria border rounded">
      </a>
    </div>


    <p>
      Diam quis tincidunt facilisis, sem quam luctus augue, ut posuere
      neque sem vitae neque.
    </p>
    <p>
      Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
      ridiculus mus. Nunc faucibus posuere turpis. Sed laoreet, est sed gravida
      tempor, nibh enim fringilla quam, et dapibus mi enim sit amet risus. Nulla
      sollicitudin eros sit amet diam. Aliquam ante. Vestibulum ante ipsum primis in
      faucibus orci luctus et ultrices posuere cubilia Curae; Ut et est. Donec semper
      nulla in ipsum. Integer elit. In pharetra lorem vel ante.
    </p>

    <p>
      Sed sed justo. Curabitur consectetuer arcu. Etiam placerat est eget odio. Nulla
      facilisi. Nulla facilisi. Mauris non neque. Suspendisse et.
    </p>


  </div>
@endsection
@push('script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
  <script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
  </script>
@endpush
