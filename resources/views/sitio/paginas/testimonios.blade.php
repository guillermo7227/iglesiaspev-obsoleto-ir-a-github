@extends('layout.master')
@section('titulo', 'Testimonios')
@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
@endpush
@section('contenido')
  <div class="container my-5">
    <h1 class="text-center text-primary font-weight-bold">TESTIMONIOS</h1>

    <p>
    Facilisi.

    Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming
    id quod mazim placerat facer possim assum. Typi non habent claritatem insitam;
    est usus legentis in iis qui facit eorum claritatem. Investigationes
    demonstraverunt lectores legere me lius quod ii.

    </p>

    <hr class="my-5">

    @foreach($testimonios as $testimonio)

      <div>

          <h2 class="text-primary">Testimonio {{ $testimonio->nombre }}</h2>

          <p class="mt-3 mb-0">
            {{ $testimonio->descripcion }}
          </p>

          <a href="{{ $testimonio->video }}"
             data-toggle="lightbox"
             data-gallery="galeria-testimonios">
            <div class="miniatura-video border rounded" style="background: url(https://i1.ytimg.com/vi/{{
                                                          \Utils::getVideoIdFromUrl($testimonio->video)
                                                        }}/mqdefault.jpg)">
            </div>
          </a>


      </div>

        <hr class="my-5">
    @endforeach

  </div>
@endsection
@push('script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
  <script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
  </script>
@endpush

