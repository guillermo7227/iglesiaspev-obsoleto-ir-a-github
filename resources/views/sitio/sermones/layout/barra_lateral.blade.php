
<!-- sermones -->
<div class="seccion-lateral">

  <h5>Sermones recientes</h5>

  <ul class="list-unstyled mb-1">
    @foreach($sermonesRecientes as $sermon)
      <li>@include('parciales.sermon-span', ['mostrarIcono' => true])</li>
    @endforeach
  </ul>

  <small><a href="{{ route('sermones.sermones.index') }}">Ver mas sermones...</a></small>

</div>

<hr class="my-4">


<!-- series -->
<div class="seccion-lateral">

  <h5>Series recientes</h5>

  <ul class="list-unstyled mb-1">
    @foreach($seriesRecientes as $serie)
      <li>@include('parciales.serie-span', ['mostrarIcono' => true])</li>
    @endforeach
  </ul>

  <small><a href="{{ route('sermones.series.index') }}">Ver mas series...</a></small>

</div>

<hr class="my-4">


<!-- lideres -->
<div class="seccion-lateral">

  <h5>Líderes</h5>

  <ul class="list-unstyled mb-1">
    @foreach($lideresCreativos as $lider)
      <li>@include('parciales.lider-span', ['mostrarIcono' => true])</li>
    @endforeach
  </ul>

  <small><a href="{{ route('sermones.lideres.index') }}">Ver mas líderes...</a></small>

</div>


<hr class="my-4">

<!-- libros -->
<div class="seccion-lateral">

  <h5>Libros</h5>

  <ul class="list-unstyled mb-1">
    @foreach($cuentaLibros as $cuentaLibro)
      <li>
        @include('parciales.libro-span', [
         'libro' => $cuentaLibro->libro,
         'cuenta' => $cuentaLibro->cuenta,
         'mostrarCuenta' => true,
        ])
      </li>
    @endforeach
  </ul>

  <small>
    <a href="{{ route('sermones.libros.index') }}">
      Ver todos los libros...
    </a>
  </small>

</div>



