@extends('layout.master')
@section('contenido')
  <div class="container my-5">

    @yield('sermones_cabecera')

    <hr class="my-5">

    <div class="row">

      <!-- Columna de sermones -->
      <div class="col-md-9">

        @yield('sermones_contenido')

      </div>

      <!-- Barra lateral -->
      <div class="col-md-3">
        @include('sitio.sermones.layout.barra_lateral')
      </div>


    </div>

  </div>
@endsection

