@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Sermones por Libros')
@section('sermones_cabecera')
  <h1 class="text-center text-primary font-weight-bold">SERMONES POR LIBROS</h1>

  <p>
  Esta sección permite consultar los sermones predicados en la iglesia organizados por libro. Útil para consultar todo lo que se ha predicado
  sobre un libro en particular.
  </p>
@endsection
@section('sermones_contenido')

  @foreach ($libros as $libro)

    <h2>@include('parciales.libro-span')</h2>

    <h5>Sermones recientes</h5>

    <ul class="list-unstyled">
      @foreach ($sermones->where('libro', $libro)->take(3) as $sermon)

        <li>
          @include('parciales.sermon-span', [
            'mostrarIcono' => true,
            'mostrarLider' => true,
            'mostrarPasaje' => true,
            'mostrarFecha' => true
          ])
        </li>

      @endforeach
    </ul>

    <small>
      <a href="{{ route('sermones.libros.show', $libro) }}">Ver todos los sermones de este libro...</a>
    </small>

    <hr class="my-5">

  @endforeach

@endsection
