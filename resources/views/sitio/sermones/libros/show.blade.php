@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Libro ' . $libro)
@section('sermones_cabecera')

  <h1 class="text-primary text-center font-weight-bold">{{ $libro }}</h1>

@endsection
@section('sermones_contenido')

  <p>Se encontraron {{ $sermones->total() }} sermones.</p>

  <ul class="list-unstyled">
    @foreach ($sermones as $sermon)
      <li>
        @include('parciales.sermon-span', [
          'mostrarIcono' => true,
          'mostrarLider' => true,
          'mostrarPasaje' => true,
          'mostrarFecha' => true,
        ])
      </li>
    @endforeach
  </ul>

  {{ $sermones->links() }}

@endsection
