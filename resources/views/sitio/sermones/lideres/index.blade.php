@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Sermones de Líderes')
@section('sermones_cabecera')
  <h1 class="text-center text-primary font-weight-bold">SERMONES DE LÍDERES</h1>

  <p>

En esta sección se listan los sermones organizados por predicador. De manera que puede consultar toda la gama de sermones de su predicador favorito
de la iglesia Su Palabra es Verdad.

  </p>
@endsection
@section('sermones_contenido')

  @foreach($lideres as $lider)

    <h2>
      <a href="{{ route('sermones.lideres.show', $lider->slug) }}">
        {{ $lider->nombre }}
      </a>
      <small class="text-muted">
        {{ $lider->posicion }}
      </small>
    </h2>

    <h5>Sermones recientes</h5>

    <ul class="list-unstyled">
      @foreach($lider->sermones->sortByDesc('fecha') as $sermon)
        <li>
          @include('parciales.sermon-span', [
            'mostrarIcono' => true,
            'mostrarPasaje' => true,
            'mostrarFecha' => true,
          ])
        </li>
        @break ($loop->index == 2)
      @endforeach
    </ul>

    <small>
      <a href="{{ route('sermones.lideres.show', $lider->slug) }}">
        Ver todos lo sermones de este líder...
      </a>
    </small>

    <hr class="my-5">

  @endforeach

@endsection
