@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Líder ' . $lider->nombre)
@section('sermones_cabecera')

  <h1 class="text-primary text-center font-weight-bold">{{ $lider->nombre }}</h1>
  @if(\Auth::check())
    <div class="w-100 text-center">
      <a href="{{ route('admin.lideres.edit', $lider->slug) }}">
        <i class="fa fa-edit"></i> Editar este líder
      </a>
    </div>
  @endif


@endsection
@section('sermones_contenido')

  <div class="row">
    <div class="col-md-3">
      <img class="img-fluid my-2 border rounded" src="{{ asset($lider->imagen) }}">

      <p class="text-muted text-center w-100">
        @if (isset($lider->contactoArr['facebook']))
          <a href="https://fb.me/{{ $lider->contactoArr['facebook'] }}" target="_blank">
            <i class="fab fa-facebook mr-2"></i>
          </a>
        @endif

        @if (isset($lider->contactoArr['instagram']))
          <a href="https://instagram.com/{{ $lider->contactoArr['instagram'] }}" target="_blank">
            <i class="fab fa-instagram mr-2"></i>
          </a>
        @endif

        @if (isset($lider->contactoArr['email']))
          <a href="mailto:{{ $lider->contactoArr['email'] }}">
            <i class="fa fa-envelope"></i>
          </a>
        @endif
      </p>
    </div>

    <div class="col-md-9">
      <p class="mb-0">
        <span class="text-muted">Posición:</span>
        {{ $lider->posicion }}
      </p>
      <p>
        {{ $lider->bio }}
      </p>
    </div>
  </div>

  <h2>Sermones</h2>

  <ul class="list-unstyled">
    @foreach($lider->sermones->sortByDesc('fecha') as $sermon)
      <li class="mb-2">
        @include('parciales.sermon-span', [
          'mostrarIcono' => true,
          'mostrarPasaje' => true,
          'mostrarFecha' => true,
        ]) <br>
        <small class="text-semimuted ml-3">
          Serie:
          @include('parciales.serie-span', [
            'serie' => $sermon->serie,
          ])
        </small>
      </li>
    @endforeach
  </ul>


@endsection
