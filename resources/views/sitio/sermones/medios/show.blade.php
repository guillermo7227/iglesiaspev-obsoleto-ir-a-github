@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Medios ' . $medio->descripcion)
@section('sermones_cabecera')

  <h1 class="text-primary text-center font-weight-bold">{{ $medio->descripcion }}</h1>
  @if(\Auth::check())
    <div class="w-100 text-center">
      <a href="{{ route('admin.medios.edit', $medio->slug) }}">
        <i class="fa fa-edit"></i> Editar este medio
      </a>
    </div>
  @endif

@endsection
@section('sermones_contenido')

  <p>
    <span class="text-muted">Tipo: </span>
    {{ $medio->tipo }}
  </p>

  <p>
    <span class="text-muted">Pertenece al sermón: </span>
    @if ($medio->sermon)
      @include('parciales.sermon-span', [
        'sermon' => $medio->sermon,
      ])
    @else
      Ninguno
    @endif
  </p>

  @include('parciales.medio-display')

@endsection
