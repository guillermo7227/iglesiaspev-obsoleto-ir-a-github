@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Series de Sermones')
@section('sermones_cabecera')
  <h1 class="text-center text-primary font-weight-bold">SERIES DE SERMONES</h1>

  <p>
  Las series son un grupo de sermones predicados sobre un mismo tema. Estos pueden estar a cargo de uno o varios pastores.
  En esta sección puede ver las series organizadas y con sus sermones consultables de manera fácil.
  </p>
@endsection
@section('sermones_contenido')

  @foreach ($series as $serie)

    <h2>{{ $serie->titulo }}</h2>

    <p class="mb-0">
      <span class="text-muted">Libros:</span>
      @foreach ($serie->getLibros() as $libro)
        @include('parciales.libro-span'){{ $loop->iteration == 1 ? '-' : '' }}
        @if ($loop->iteration == 2 && $loop->remaining > 0)
          y {{ $loop->remaining }} mas.
          @break
        @endif
      @endforeach
    </p>

    <p class="mb-0">
      <span class="text-muted">Predicada por:</span>
      @foreach ($serie->getLideres() as $lider)
        @include('parciales.lider-span'){{ $loop->iteration == 1 ? '-' : '' }}
        @if ($loop->iteration == 2 && $loop->remaining > 0)
          y {{ $loop->remaining }} mas.
          @break
        @endif
     @endforeach
    </p>

    <p class="mb-0 text-semimuted">{{ $serie->descripcion }}</p>

    <p class="mb-0 text-muted">Sermones de esta serie:</p>
    <ul class="list-unstyled">
      @foreach ($serie->sermones as $sermon)
        <li>&ensp;@include('parciales.sermon-span', ['mostrarIcono' => true])</li>
        @if ($loop->iteration == 3)
          <a href="{{ route('sermones.series.show', $serie->slug) }}">
            <!-- <i class="fa fa&#45;sm fa&#45;arrow&#45;circle&#45;right"></i> -->
            <small>Ver todos los sermones de esta serie... ({{ $loop->remaining }} mas)</small>
          </a>
          @break
        @endif
      @endforeach
    </ul>

    <hr class="my-5">

  @endforeach


  {{ $series->links() }}

@endsection
