@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Serie ' . $serie->titulo)
@section('sermones_cabecera')

  <h1 class="text-primary text-center font-weight-bold">{{ $serie->titulo }}</h1>
  @if(\Auth::check())
    <div class="w-100 text-center">
      <a href="{{ route('admin.series.edit', $serie->slug) }}">
        <i class="fa fa-edit"></i> Editar esta serie
      </a>
    </div>
  @endif

@endsection
@section('sermones_contenido')

  <p class="mb-0">
    <span class="text-muted">Libros en esta serie:</span>
    @foreach ($serie->getLibros() as $libro)
      @include('parciales.libro-span'){{ $loop->last ? '' : '-' }}
    @endforeach
  </p>

  <p class="mb-0">
    <span class="text-muted">Predicada por:</span>
    @foreach ($serie->getLideres() as $lider)
      @include('parciales.lider-span'){{ $loop->last ? '' : '-' }}
   @endforeach
  </p>

  <p class="text-semimuted">{{ $serie->descripcion }}</p>

  <p class="mb-0 text-muted">Sermones de esta serie:</p>
  <ul class="list-unstyled">
    @foreach ($serie->sermones->sortBy('fecha') as $sermon)
      <li class="mb-2">&ensp;
        @include('parciales.sermon-span', ['mostrarIcono' => true])
        <br>&emsp;&ensp;
        <small>
          <span class="text-muted">Por:</span> {{ $sermon->lider->nombre }}
          <span class="text-muted">Pasaje:</span> {{ $sermon->pasaje }}
          <span class="text-muted">Fecha:</span> {{ $sermon->fechaFormateada }}
        </small>
      </li>
    @endforeach
  </ul>

  <hr class="my-5">

@endsection
