@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Sermones')
@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
@endpush

@section('sermones_cabecera')
    <h1 class="text-center text-primary font-weight-bold">SERMONES</h1>
    <p>
    Los pastores y maestros de nuestra iglesia predican todo el consejo de la Palabra de Dios expositivamente, imitando la metodología de enseñanza usada por los apóstoles.
    Queremos presentar la palabra del Dios vivo tal cual es revelada en las Escrituras procurando una exégesis e interpretación bíblicas. 
    En esta sección encontrará los sermones predicados en nuestra iglesia domingo tras domingo para la gloria de Dios y edificación de los santos.
    </p>
@endsection

@section('sermones_contenido')

  @if(count($sermones) == 0)
    <p class="text-center">No hay sermones que mostrar</p>
  @endif

  @foreach($sermones as $sermon)

    <h2>
      <a href="{{ route('sermones.sermones.show', $sermon->slug) }}">
        {{ $sermon->titulo }}
      </a>
    </h2>

    <div class="row">
      <div class="col-lg-7">
        <p class="mb-0">
          <span class="text-muted">Por:</span>
          @include('parciales.lider-span', ['lider' => $sermon->lider])
          <span class="text-muted">Fecha:</span> {{ $sermon->fechaFormateada }}
        </p>

        <p class="mb-0">
          <span class="text-muted">Serie:</span>
          @include('parciales.serie-span', ['serie' => $sermon->serie])
          <span class="text-muted">Pasaje:</span>
          @include('parciales.pasaje-span', ['libro' => $sermon->libro])
        </p>

        <p class="text-semimuted">{{ $sermon->descripcion }}</p>

      </div>
      <div class="col-lg-5">
        @switch($sermon->medio->tipo)
        @case('Documento')
          <p>
            {{ $sermon->medio->tipo }}:
            <a href="{{ $sermon->medio->url }}" target="_blank">{{ $sermon->medio->url }}</a>
          </p>
          @break

        @case('Audio')
          <p> <audio src="{{ $sermon->medio->url }}" controls></audio></p>
          @break

        @case('Video')
          <a href="{{ $sermon->medio->url }}" data-toggle="lightbox">
            <div class="miniatura-video border rounded" style="background: url(https://i1.ytimg.com/vi/{{
                                                          \Utils::getVideoIdFromUrl($sermon->medio->url)
                                                        }}/mqdefault.jpg)">
            </div>
          </a>
          @break
        @endswitch
      </div>

    </div>
    <hr class="my-5">

  @endforeach

  {{ $sermones->links() }}

@endsection

@push('script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
  <script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
  </script>
@endpush

