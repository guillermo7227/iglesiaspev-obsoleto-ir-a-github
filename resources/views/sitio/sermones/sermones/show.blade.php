@extends('sitio.sermones.layout.sermones_master')
@section('titulo', 'Sermón ' . $sermon->titulo)
@section('sermones_cabecera')

  <h1 class="text-primary text-center font-weight-bold">{{ $sermon->titulo }}</h1>
  @if(\Auth::check())
    <div class="w-100 text-center">
      <a href="{{ route('admin.sermones.edit', $sermon->slug) }}">
        <i class="fa fa-edit"></i> Editar este sermón
      </a>
    </div>
  @endif

@endsection
@section('sermones_contenido')

  <p class="mb-0">
    <span class="text-muted">Por:</span>
    @include('parciales.lider-span', ['lider' => $sermon->lider])
    <span class="text-muted">Fecha:</span> {{ $sermon->fechaFormateada }}
  </p>

  <p class="mb-0">
    <span class="text-muted">Serie:</span>
    @include('parciales.serie-span', ['serie' => $sermon->serie])
    <span class="text-muted">Pasaje:</span>
    @include('parciales.pasaje-span', ['libro' => $sermon->libro])
  </p>

  <p class="text-semimuted">{{ $sermon->descripcion }}</p>

  @include('parciales.medio-display', [
    'medio' => $sermon->medio
  ])

@endsection
