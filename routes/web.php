<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// sitio
Route::group([
    'prefix' => '/',
    'as' => 'sitio.',
], function() {

    Route::get('', 'SitioController@inicio')->name('inicio');
    Route::get('ministerios', 'SitioController@ministerios')->name('ministerios');
    Route::get('lo_que_ensenamos', 'SitioController@loQueEnsenamos')->name('lo_que_ensenamos');
    Route::get('lideres', 'SitioController@lideres')->name('lideres');
    Route::get('testimonios', 'SitioController@testimonios')->name('testimonios');
    Route::get('contacto', 'SitioController@contacto')->name('contacto');
    Route::post('contacto', 'SitioController@mensajeContacto')->name('mensajeContacto');
    Route::post('boletin', 'SitioController@suscribirseBoletin')->name('suscribirse_boletin');
    Route::delete('boletin', 'SitioController@desuscribirseBoletin')->name('desuscribirse_boletin');

    Route::get('test', 'SitioController@test')->name('test');
});


// seccion de sermones
Route::group([
    'prefix' => 'sermones',
    'as' => 'sermones.',
], function() {

    // sermones
    Route::redirect('', config('app.url').'/sermones/sermones');
    Route::group([
        'prefix' => 'sermones',
        'as' => 'sermones.',
    ], function() {

        Route::get('', 'SermonController@index')->name('index');
        Route::get('{slug}', 'SermonController@show')->name('show');
    });

    // series
    Route::group([
        'prefix' => 'series',
        'as' => 'series.',
    ], function() {

        Route::get('', 'SerieController@index')->name('index');
        Route::get('{slug}', 'SerieController@show')->name('show');
    });

    // lideres
    Route::group([
        'prefix' => 'lideres',
        'as' => 'lideres.',
    ], function() {

        Route::get('', 'LiderController@index')->name('index');
        Route::get('{slug}', 'LiderController@show')->name('show');
    });

    // libros
    Route::group([
        'prefix' => 'libros',
        'as' => 'libros.',
    ], function() {

        Route::get('', 'LibroController@index')->name('index');
        Route::get('{libro}', 'LibroController@show')->name('show');
    });

    // medios
    Route::group([
        'prefix' => 'medios',
        'as' => 'medios.',
    ], function() {

        Route::get('{slug}', 'MedioController@show')->name('show');
    });

});


// admin
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['auth'],
    'namespace' => 'Admin',
], function() {

    Route::get('/', 'EscritorioController@index')->name('index');
    Route::get('/activar-transmision', 'EscritorioController@activarTransmision')->name('activar-transmision');


    // sermones
    Route::group([
        'prefix' => 'sermones',
        'as' => 'sermones.',
    ], function() {

        Route::get('', 'ASermonController@index')->name('index');
        Route::get('{slug}/edit', 'ASermonController@edit')->name('edit');
        Route::put('{slug}', 'ASermonController@update')->name('update');
        Route::get('create', 'ASermonController@create')->name('create');
        Route::post('', 'ASermonController@store')->name('store');

        // Ajax
        Route::get('todos', 'ASermonController@apiTodos')->name('apiTodos');
        Route::delete('{slug}', 'ASermonController@apiDestroy')->name('apiDestroy');
    });


    // series
    Route::group([
        'prefix' => 'series',
        'as' => 'series.',
    ], function() {

        Route::get('', 'ASerieController@index')->name('index');
        Route::get('{slug}/edit', 'ASerieController@edit')->name('edit');
        Route::put('{slug}', 'ASerieController@update')->name('update');
        Route::get('create', 'ASerieController@create')->name('create');
        Route::post('', 'ASerieController@store')->name('store');

        // Ajax
        Route::get('todos', 'ASerieController@apiTodos')->name('apiTodos');
        Route::delete('{slug}', 'ASerieController@apiDestroy')->name('apiDestroy');
    });


    // lideres
    Route::group([
        'prefix' => 'lideres',
        'as' => 'lideres.',
    ], function() {

        Route::get('', 'ALiderController@index')->name('index');
        Route::get('{slug}/edit', 'ALiderController@edit')->name('edit');
        Route::put('{slug}', 'ALiderController@update')->name('update');
        Route::get('create', 'ALiderController@create')->name('create');
        Route::post('', 'ALiderController@store')->name('store');

        // Ajax
        Route::get('todos', 'ALiderController@apiTodos')->name('apiTodos');
        Route::delete('{slug}', 'ALiderController@apiDestroy')->name('apiDestroy');
    });

    // medios
    Route::group([
        'prefix' => 'medios',
        'as' => 'medios.',
    ], function() {

        Route::get('', 'AMedioController@index')->name('index');
        Route::get('{slug}/edit', 'AMedioController@edit')->name('edit');
        Route::put('{slug}', 'AMedioController@update')->name('update');
        Route::get('create', 'AMedioController@create')->name('create');
        Route::post('', 'AMedioController@store')->name('store');

        // Ajax
        Route::get('todos', 'AMedioController@apiTodos')->name('apiTodos');
        Route::delete('{slug}', 'AMedioController@apiDestroy')->name('apiDestroy');
    });
});


Auth::routes();

